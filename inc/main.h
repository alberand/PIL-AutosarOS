#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "sample_hw.h"

#if defined(OSDIABPPC)
#define APP_NEAR_16                __attribute__(( section(".sapp_v16_data") ))
#define APP_NEAR_32                __attribute__(( section(".sapp_v32_data") ))
#define APP_NEAR_U                 __attribute__(( section(".sapp_vU_data") ))
#endif

/* Global 'receive' application data */
#define SMP_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
extern APP_NEAR_16 volatile unsigned short maintask_c, simulationtask_c, extmodetask_c;
#define SMP_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define SMP_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
extern APP_NEAR_16 volatile unsigned short overrun_flag, overrun_detected;
#define SMP_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define SMP_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
extern APP_NEAR_16 volatile unsigned short simulation_finished, led_tgl, is_running;
#define SMP_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define INTC_PSR(n)          (INTC_BASEADDR + ((uint32)0x60u) + (2*n))

#ifdef __cplusplus
}
#endif

#endif  /* MAIN_H */