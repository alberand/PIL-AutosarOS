#ifndef SAMPLEHW_H
#define SAMPLEHW_H
/************************************************************************************** 
*
*   Freescale(TM) and the Freescale logo are trademarks of Freescale Semiconductor, Inc.
*   All other product or service names are the property of their respective owners.
*   (c) Copyright 2014 - 2015 Freescale Semiconductor Inc.
*   All Rights Reserved.
*
*   You can use this example for any purpose on any computer system with the
*   following restrictions:
*
*   1. This example is provided "as is", without warranty.
*
*   2. You don't remove this copyright notice from this example or any direct derivation
*      thereof.
*
*  Description:  AUTOSAR OS sample application
*
**************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#define SMPL_REG32( address )      ( *(volatile OSDWORD*)(address) ) /**<  32-bit register */
#define SMPL_REG16( address )      ( *(volatile OSWORD *)(address) ) /**<  16-bit register */
#define SMPL_REG8( address )       ( *(volatile OSBYTE *)(address) ) /**<  8-bit  register */

/******************************************************************** BASE ADDRESSES **********************************************************/

#define SWT0_BASE_ADDR	(0xFC050000UL)
#define GPIO_BASE_ADDR  (0xFFFC0000UL)
#define ME_BASE_ADDR    (0xFFFB8000UL)
#define RGM_BASE_ADDR   (0xFFFA8000UL)
#define GCM_BASE_ADDR	(0xFFFB0000UL)

/******************************************************************** GPIO ********************************************************************/
/* NOTE: GPIO_NUM is connected to LED1 on EVB motherboard */

#define GPIO_NUM         24
#define GPIO_MSCR_NUM    98

#define SIUL2_MSCR(n)  SMPL_REG32(GPIO_BASE_ADDR + 0x240 + GPIO_MSCR_NUM * 4 + (n) * 4)      /* SIUL2 Multiplexed Signal Configuration Register */
/* GPDO register uses:
 * PDO_4n2: to set/reset LED1
 * PDO_4n3: to set/reset LED2
 * */
#define SIUL2_GPDO(n)  SMPL_REG8(GPIO_BASE_ADDR + 0x1300 + GPIO_NUM * 4 + 2 + (n))           /* Pad Data Output Register */
#define MSCR         0x02000000ul                                      /* GPIO Output Buffer Enable */

#define SetGPIO(n)    SIUL2_GPDO(n)  = 0x1       /* set pin GPIO_NUM */
#define ClrGPIO(n)    SIUL2_GPDO(n)  = 0x0       /* reset pin GPIO_NUM */
#define ToogleGPIO(n) SIUL2_GPDO(n) ^= 0x1       /* toogle pin GPIO_NUM */

#define InitGPIO(n)        \
    SIUL2_MSCR(n) = MSCR;  \
    ClrGPIO(n)

/******************************************************************** Watchdog ********************************************************************/

#define SWT0_SR_R  SMPL_REG32(SWT0_BASE_ADDR + 0x10)
#define SWT0_CR_R  SMPL_REG32(SWT0_BASE_ADDR)

/** Access the WatchDog only from the first CPU, the others might not have access to SWT0 **/
#define DisableWatchdog() do {                \
    if (OSGetCoreID() == OS_CORE_ID_MASTER) { \
        SWT0_SR_R = 0xc520;                   \
        SWT0_SR_R = 0xd928;                   \
        SWT0_CR_R = 0x8000010A;               \
    }                                         \
} while (0)

/******************************************************************** Clocks ********************************************************************/

#define CGM_SC_DC0_3 (SMPL_REG32(GCM_BASE_ADDR + 0x7E8))           /* System Clock Divider Configuration Registers (CGM_SC_DC0:3) */
#define ME_GS        (SMPL_REG32(ME_BASE_ADDR))         /* Global Status Register */
#define ME_RUN_PC    (SMPL_REG32(ME_BASE_ADDR + 0x80))  /* Run Peripheral Configuration Registers MC_ME_RUN_PC0 */
#define ME_MCTL      (SMPL_REG32(ME_BASE_ADDR + 0x4))   /* Mode Control Register */
#define MC_ME_CCTL(coreId) \
    (SMPL_REG16(ME_BASE_ADDR + 0x1C6 + ((coreId) << 1UL)))     /* Core Control Register for core with given coreId */

#define EnableAllPeriph()    \
{                            \
    DisableWatchdog();       \
    CGM_SC_DC0_3 = 0x80000000;       /* Divider 2 Enable,DIV0 = 1 */  \
    ME_RUN_PC = 0x000000FE;          /* Set configuration,peripheral ON in every mode */ \
    ME_MCTL = 0x30005AF0;            /* Re-enter in DRUN mode to update with control key */ \
    ME_MCTL = 0x3000A50F;            /* --//-- with inverted control key */ \
    while(ME_GS & 0x08000000ul){}    /* Wait for mode transition to complete */ \
    while((ME_GS & 0x30000000ul) != 0x30000000ul){}     /* Check current mode status is DRUN */ \
}

/* Reset generation module */
#define RGM_FES (SMPL_REG16(RGM_BASE_ADDR)) /* Functional Event Status */


#ifdef __cplusplus
}
#endif

#endif  /* SAMPLEHW_H */
