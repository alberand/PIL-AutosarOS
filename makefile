include Makefile.config

# User Application sources
CC_FILES_TO_BUILD += $(PROJECT_ROOT)src/main.c 

# set final target
.PHONY: all
all: $(OUTPUT_PATH) $(BIN_OUTPUT_PATH)\$(projname).elf

include Makefile.rules

#################################################################
# Rule for linking
$(BIN_OUTPUT_PATH)\$(projname).elf: $(OBJS)
	@$(ECHO) Linking $@
	@$(MKDIR) -p $(bin_output_path)
	$(LD) $(LDFLAGS) $(call b2f,$(OBJS)) > $(bin_output_path)/$(projname).map -o $(subst \,/,$@)

#################################################################
# Upload. Use PE Micro GDB server shipped with S32 studio
#################################################################
# Check if it is already running
GDB_STATUS := $(shell tasklist | grep "pegdbserver_power_console")
upload: $(FLASH_SCR) #  $(BIN_OUTPUT_PATH)\$(projname).elf
ifeq ($(GDB_STATUS),)
	# Run PEMicro debug server
	$(S32_ROOT)/eclipse/plugins/com.pemicro.debug.gdbjtag.ppc_1.7.2.201709281658/win32/pegdbserver_power_console $(PEGDB_FLAGS) &
endif
	# Run gdb
	$(GDB) --batch --command=$(FLASH_SCR)

# Generate GDB script
$(FLASH_SCR):
	touch $(FLASH_SCR)
	@$(ECHO) "target remote localhost:7224" > $(FLASH_SCR)
	@$(ECHO) "monitor _reset" >> $(FLASH_SCR)
	@$(ECHO) "load " $(call b2f,$(BIN_OUTPUT_PATH)\$(projname).elf) >> $(FLASH_SCR)
	# @$(ECHO) "file " $(call b2f,$(BIN_OUTPUT_PATH)\$(projname).elf) >> $(FLASH_SCR)
	@$(ECHO) "detach" >> $(FLASH_SCR)

#################################################################
# Addition commands
#################################################################
# Target for show internal options
showflags:
	@$(ECHO) showflags...
	@$(ECHO) COMPILEROPTIONS = $(CFLAGS)
	@$(ECHO) LINKEROPTIONS = $(LDFLAGS)
	@$(ECHO) PREPROCESSOR_DEFINES = $(PREPROCESSOR_DEFINES)
	# @$(ECHO) CC_FILES_TO_BUILD = 
	# @$(foreach file,$(CC_FILES_TO_BUILD),$(ECHO) '    $(file)';) #$(CC_FILES_TO_BUILD)
	@$(ECHO) ASM_FILES_TO_BUILD = 
	@$(foreach file,$(ASM_FILES_TO_BUILD),$(ECHO) '    $(file)';) #$(ASM_FILES_TO_BUILD)
	@$(ECHO) CC_INCLUDE_PATH = 
	@$(foreach file,$(CC_INCLUDE_PATH),$(ECHO) '    $(file)';) #$(CC_INCLUDE_PATH)
	@$(ECHO) $(MOD_FOLDERS)
	# @$(ECHO) FILES_TO_LINK = 
	# @$(foreach file,$(FILES_TO_LINK),$(ECHO) '    $(file)';) #$(FILES_TO_LINK)

clean:
	@$(ECHO) Clean...
	@$(RM) -fR $(OUTPUT_PATH)