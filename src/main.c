#include <Os.h>

#include "typedefs.h"
#include "Std_Types.h"
#include "Mcal.h"
#include "Mcal_Os.h"

#include "modules.h"
#include "CDD_Mcl.h"

#include "Pwm_Cfg.h"

#include "Mcu.h"
#include "EcuM.h"

#include "Port.h"
#include "Dio.h"
#include "Adc.h"
#include "Pwm.h"
#include "Mcl.h"

#include "SchM_Dio.h"
#include "SchM_Port.h"
#include "SchM_Mcu.h"
#include "SchM_Adc.h"
#include "SchM_Pwm.h"
#include "SchM_Mcl.h"

#include "main.h"
#include "uart.h"

/* Global 'receive' application data */
#define SMP_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
APP_NEAR_16 volatile unsigned short maintask_c, simulationtask_c, extmodetask_c;
#define SMP_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define SMP_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
APP_NEAR_16 volatile unsigned short overrun_flag, overrun_detected;
#define SMP_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define SMP_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
APP_NEAR_16 volatile unsigned short simulation_finished, led_tgl, main_run, is_running;
#define SMP_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define APP_START_SEC_CODE
#include "MemMap.h"

/* Tests. Comment (undefine) to disable */
#define UART_ECHO_TEST
// #define OVERRUN_TEST
// #define ADC_TEST
// #define PWM_TEST
// #define STM0_OVERVIEW
#define FREQ_TEST

#define STEP_SIZE_MILLIS (0.0005*1000)
#define CHECK_EVERY_N_RUN (STEP_SIZE_MILLIS*2 - 1) // division by 0.5 (0.5 ms)

#define ADC_MAX_VALUE (1 << 12)
#define PWM_DUTY_CYCLE_MAX_VALUE 32768

/*! @breif Hardware and Autosar OS intialization
 * 
 */
void main( void )
{
    // Task's control variables
    maintask_c        = 0;
    simulationtask_c  = 0;
    extmodetask_c     = 0;
    is_running        = 0;

    main_run            = 0;
    // LEDTASK overrun flag
    overrun_flag        = 0;
    // Global overrun 
    overrun_detected    = 0;
    // Testing var
    simulation_finished  = 0;
    led_tgl = STD_HIGH;

#if (USE_MCU_MODULE==STD_ON)
    // MCU initialization
    Mcu_Init(&McuModuleConfiguration_0);
    Mcu_SetMode(McuModeSettingConf_0);
    Mcu_InitClock(McuConf_McuClockSettingConfig_McuClockSettingConfig_0);
#endif

    // Init Ecu manager
    EcuM_SetWakeupEvent(0);
    EcuM_ValidateWakeupEvent(0);
    EcuM_CheckWakeup(0);

    //RGM_FES = 0x4080; // clear fccr_hard and fccu_safe flags 
    EnableAllPeriph(); 

#if (USE_PORT_MODULE==STD_ON)
    Port_Init(&PortConfigSet_0);
#endif

#if (USE_MCL_MODULE==STD_ON)
    Mcl_Init(&MclConfigSet_0);
#endif

#if (USE_ADC_MODULE==STD_ON)
    /* Adc Interrupts */
    // REG_WRITE16(INTC_PSR(548), 0x8007);   /* Adc_EndGroupConvUnit0 */
    REG_WRITE16(INTC_PSR(554), 0x8007);   /* Adc_EndGroupConvUnit1 */
    /* Initialize the ADC with the post build configuration pointer*/
    Adc_Init((Adc_ConfigType *)(&AdcConfigSet_0));
#endif

#if (USE_PWM_MODULE==STD_ON)
    /* Pwm Interrupts */
    REG_WRITE16(INTC_PSR(706), 0x8007);    /* EMIOS_0_CH_0_CH_1_ISR */
    /* Initialize the PWM with the post build configuration pointer*/
    Pwm_Init((Pwm_ConfigType *)(&PwmChannelConfigSet_0));
#endif

    InitCounter(OsCounter_0, 1);
    InitCounter(OsCounter_1, 1);

    // Initialization of INT for UART
    REG_WRITE16(INTC_PSR(382), 0x8007);    // LINFLEX_SCI_Isr_UART_RX 
    REG_WRITE16(INTC_PSR(383), 0x8007);    // LINFLEX_SCI_Isr_UART_TX 
    (*(volatile uint32*)(0xFC040010))= (uint32)(0); // Ensure INTC's current priority is 0 


    // Initalize UART
    uart_console_init();

    // Jump to Os startup
    StartOS( Mode01 );
}

/*! @brief Run Simulation task, detects overruns
 *
 */
TASK( MAINTASK )
{
    CONSOLE_MESSAGE("[MAINTASK] Task started\n");

    while(!simulation_finished){
        WaitEvent(MainActEvent);

        maintask_c++;

        if(Dio_ReadChannel((Dio_ChannelType)DioConf_DioChannel_SW5) == STD_HIGH)
        {
            Dio_WriteChannel((Dio_ChannelType)DioConf_DioChannel_LED4, 
                             (Dio_LevelType)STD_LOW);
        } 
        else
        {
            Dio_WriteChannel((Dio_ChannelType)DioConf_DioChannel_LED4, 
                             (Dio_LevelType)STD_HIGH);
        }

        // Use only every CHECK_EVERY_N_RUN cycle for overrun check and simulation
        // task run
        if(main_run != 0){
            ClearEvent(MainActEvent);
            main_run--;
            continue;
        }
        
        main_run = (uint16_t)CHECK_EVERY_N_RUN;

        // Overrun detection
        if (overrun_flag == 1){
            // CONSOLE_MESSAGE("[MAINTASK] Overrun detected\n");
            sci_a_putchar('o');
            sci_a_putchar('\n');
            // At this point we should report an error
            overrun_detected = 1;
            // Reset overrun flag
            overrun_flag = 0;
        }

        // Activate Simulation task
        SetEvent(SIMULATIONTASK, SimActEvent);

        // TODO: Should be moved into ApplicationStartupHook
        // Set STM0's source FXOSC. Didn't found in Tresos
        REG_BIT_SET32(STM_0_BASEADDR, BIT4);

        ClearEvent(MainActEvent);
    }

    (void)TerminateTask();
}

TASK( EXTMODETASK )
{
    CONSOLE_MESSAGE("[EXTMODETASK] Task started\n");

    // There should be Matlab autogenerated code for External Mode

    (void)TerminateTask();
}

TASK( SIMULATIONTASK )
{
    unsigned int i;
    char msg[1] = { '\n' };
    uint16_t dc;
    uint16_t AdcResultBufferPointer[1];

    CONSOLE_MESSAGE("[SIMULATIONTASK] Task started\n");
    while(!simulation_finished){

        simulationtask_c++;

        WaitEvent(SimActEvent);

        // Set Overrun flag
        overrun_flag = 1;

        // TASK BODY

#ifdef UART_ECHO_TEST
        if(uart_recv(msg, 1) == 1){
            uart_send(msg, 1);
        }
#endif /* ifdef UART_ECHO_TEST */

#ifdef OVERRUN_TEST
    // Wait while button is pressed
    while(Dio_ReadChannel((Dio_ChannelType)DioConf_DioChannel_SW4) == STD_HIGH){
        ;
    }
#endif /* ifdef OVERRUN_TEST */

#ifdef ADC_TEST
    if (is_running == 0)
    {
        is_running = 1;
        Adc_SetupResultBuffer(AdcConf_AdcGroup_AdcGroup_0, AdcResultBufferPointer);

        Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
    }

    // Wait until conversion is complete
    while(ADC_STREAM_COMPLETED != Adc_GetGroupStatus(AdcConf_AdcGroup_AdcGroup_0)){
        ;
    }
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);

    if (ADC_IDLE == Adc_GetGroupStatus(AdcConf_AdcGroup_AdcGroup_0) && is_running == 1)
    {
        is_running = 0;
        // CONSOLE_MESSAGE("[ADC_TEST] ADC value: %d\n", AdcResultBufferPointer[AdcChannel_0]);
    }
#endif /* ifdef ADC_TEST */

#ifdef PWM_TEST
#ifndef ADC_TEST
    CONSOLE_MESSAGE("Please enable ADC test (by defining ADC_TEST constant) to enable PWM test\n")
#endif /* ifndef ADC_TEST */	

    // Stupid check. Change value every 5-th task creation
    if(simulationtask_c % 5 == 0){
        dc = (AdcResultBufferPointer[AdcChannel_0] * PWM_DUTY_CYCLE_MAX_VALUE)/ADC_MAX_VALUE;
        //CONSOLE_MESSAGE("[PWM_TEST] Setting DC: %d\n", dc);
        Pwm_SetDutyCycle(PwmConf_PwmChannelConfigSet_Pwm_Led1, dc);
    }

#endif /* ifdef PWM_TEST */

#ifdef STM0_OVERVIEW
    if(simulationtask_c % 50 == 0)
    {
        CONSOLE_MESSAGE("Information about STM0:\n");
        // STM0 Control Register
        CONSOLE_MESSAGE("STM0_CR: 0x%08x;\n", (uint32_t)REG_READ32(STM_0_BASEADDR));
        // STM0 Channel 0 Control Register
        CONSOLE_MESSAGE("STM0_CCR0: 0x%08x;\n", (uint32_t)REG_READ32(STM_0_BASEADDR + 0x10));
        // STM0 Channel 0 Compare Register
        CONSOLE_MESSAGE("STM0_CMP0: 0x%08x;\n", (uint32_t)REG_READ32(STM_0_BASEADDR + 0x18));
    }
#endif /* ifdef STM0_OVERVIEW */

#ifdef FREQ_TEST
        Dio_WriteChannel((Dio_ChannelType)DioConf_DioChannel_FreqTest, (Dio_LevelType)led_tgl);
        if(led_tgl == STD_HIGH){
            led_tgl = STD_LOW;
        }else{
            led_tgl = STD_HIGH;
        }
#endif

        // TASK BODY END

        overrun_flag = 0;
        ClearEvent(SimActEvent);

    } /* while(!simulation_finished) */

    (void)TerminateTask();
}

#define APP_STOP_SEC_CODE
#include "MemMap.h"
