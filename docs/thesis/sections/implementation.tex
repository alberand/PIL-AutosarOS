\chapter{Implementation Details}

\par Chapter \ref{ch:design} explains the design of the developed system. This
chapter gives description of implementation details of the project.  The result
of this work is distributed in the form of a folder with all configurations and
sources included. However, this is not stand-alone working example and requires
additional software described in chapter \ref{ch:env}.

%===============================================================================
%===============================================================================
\section{Folders and Files description}

%===============================================================================
\subsection{Folders structure}

\par The following listing shows folder structure of the developed system. We
separate the project into two parts -- one contains all the content necessary for
the developing and running firmware on the target hardware and second one for
integration with Simulink. 

\begin{figure}[H]
  \begin{minipage}{\textwidth}
  \dirtree{%
  .1 ./.
    .2 cfg\DTcomment{PE Micro \acrshort{gdb} server configuration}.
    .2 docs\DTcomment{Documentation}.
    .2 inc\DTcomment{Headers}.
    .2 lcf\DTcomment{Linker Script}.
    .2 PIL-example\DTcomment{EB Tresos Studio Project}.
      .3 config\DTcomment{Folder with Autosar configs in *.xdm}.
      .3 output\DTcomment{Generated Output}.
        .4 generated.
          .5 epc\DTcomment{Autosar OS configuration}.
          .5 include\DTcomment{Modules headers}.
          .5 output\DTcomment{Modules configuration in *.epc}.
          .5 src\DTcomment{Modules sources}.
    .2 simulink\DTcomment{Simulink related content}.
      .3 blocks\DTcomment{Blocks visual and code components}.
        .4 tlc\_c\DTcomment{TLC source code of the blocks}.
      .3 demos\DTcomment{Simulink Demonstration models}.
    .2 src\DTcomment{Testing Application Sources}.
      .3 lib\DTcomment{User's libraries (UART)}.
  }
  \end{minipage}
\caption{Directory tree of the project}
\end{figure}

\par The root folder contains a few files with configuration for software tools such
as linker or \acrshort{gdb}. The other part of the folder contains sources for
a testing application.

\par The testing application means to be the application which has the same
structure as software generated by Simulink, except it doesn't have Simulink's
injections of code. This can be useful for testing newly added Autosar module or
for verification of code which hard to debbug using Simulink modeling tool.

\par Let's discuss these folders in more details:

\begin{itemize}
  \item \textbf{cfg} - contains configuration file for P\&E Micro \acrshort{gdb}
    server which was taken from the S32 Design Studio workspace.

  \item \textbf{docs} - this folder contains this document and its sources

  \item \textbf{inc}  - folder with headers used in testing and Simulink
    applications. These headers defines, for example, variable types, system
    version, UART library interface. Some of them were taken from NXP's Sample
    Application, and some were created for the testing application located at
    \path{PROJECT_ROOT/src}. Note that these headers are also used for
    application generated by Simulink.

  \item \textbf{lcf} - folder contains linker script used in the compilation
    process. This script was taken from NXP's sample application and modified
    for this project.

  \item \textbf{PIL-example} - contains EB Tresos Studio project with
    configuration used in this example. The project can be imported into Tresos
    Studio workspace. To be able to modify the project you need to install
    plug-ins shipped as part of NXP software package (see section \ref{sec:nxpswp}).

  \item \textbf{simulink} - folders with scripts, templates and models for
    integration with Simulink.

  \item \textbf{simulink/blocks} - blocks' templates and their visual models

  \item \textbf{simulink/blocks/tlc\_c} - TLC description of the blocks

  \item \textbf{simulink/demos} - prepared Simulink demonstration models

  \item \textbf{src} - testing application code sources

  \item \textbf{src/lib} - testing application libraries. It contains a library
    for work with the UART which was taken from one of the NXP's sample
    application and modified with the necessary functions for our purposes. This
    library is also used in the application generated by Simulink.
\end{itemize}

%===============================================================================
\subsection{Makefiles}\label{sec:makefiles}

\par The compilation process of the testing and Simulink's application is
controlled by Makefiles (Simulink executes them). The project root folder
contains there makefiles - makefile, Makefile.config and Makefile.rules. 

\begin{itemize}
  \item \textbf{makefile} - is a Makefile which is used only for compilation of
    testing application. This file also contains a few rules for uploading firmware
    into target hardware, cleaning the project and printing some debug
    information. This file utilizes both of configuration and rules makefiles.
  \item \textbf{Makefile.config} - this file contains the configuration of the
    compilation process. These are, for example, paths to Autosar OS, S32
    Studio, Diab compiler, compilation flags, output folder, etc. Makefile
    generated by Simulink for model compilation includes these configurations to
    replace default ones. An Autosar application compiled by this file can be
    made up of various configuration of MCAL modules. Therefore, to optimize and
    speed up the compilation process, this Makefile parses Autosar configurations
    to obtain a list of enabled MCAL modules. The file with the list of enabled
    modules is located at \path{PROJECT_ROOT/PIL-example/generated/include/modules.h}.
    The inclusion of a few key modules is hard-coded (Base, ECUM, RTE). The list
    is obtained via \textbf{Cygwin} (see \ref{sec:make}) utilities such as cat
    and grep. The code can be seen in listing \ref{lst:mcal_list}.
  \item \textbf{Makefile.rules} - this file contains rules for the compilation of
    particular files. Note that Autosar OS has separated makefiles located at
    \path{OS_DIR/mak/os_defs.mak} and \path{OS_DIR/mak/os_rules.mak}.  If you
    include this file in another makefile (e.g., in the
    \path{PROJECT_ROOT/makefile}), it should be included after Makefile.config.
    This file is used by Simulink Makefile to compile Autosar OS, MCAL
    modules and user's libs (located at \path{PROJECT_ROOT/src/lib}).
\end{itemize}

\begin{lstlisting}[
  style=makefile,
  caption=Part of the Makefile.config which obtains list of used MCAL modules,
  label={lst:mcal_list},
  escapeinside={(*@}{@*)},
]
# Get list of enabled modules
AUTOSAR_MODULES := $(shell cat "$(TRESOS_OUTPUT_FS)/include/modules.h" | grep '^\(*@\#@*)define USE_' | grep 'STD_ON' | cut -d'_' -f2)

# Base modules which are not included in modules.h
AUTOSAR_MODULES += Base
AUTOSAR_MODULES += ECUM
AUTOSAR_MODULES += RTE

# Get folders from NXP MCAL software package
MOD_FOLDERS := $(addprefix $(MCAL_TRESOS_PLUGINS)/,$(foreach mod_name,$(AUTOSAR_MODULES),$(shell ls $(MCAL_TRESOS_PLUGINS) | grep -i "$(mod_name)_")))
\end{lstlisting}

% $
%===============================================================================
\subsection{Compilation \& Execution}

\par The application generated by Simulink contains Makefile in it
(the name depends on the model's name $<modelName>.mk$). This Makefile is
generated from template \textbf{makefile.tmf} located at
\path{PROJECT_ROOT/simulink}. Template includes configuration and rules
Makefiles from the project root (see section \ref{sec:makefiles}).

\par The Wind River Diab compiler is used not only for the compilation of Autosar
sources but also for compilation of sources generated by Simulink. Due to the
fact that it is a commercial compiler, it requires a license. The license is located
at the path \path{DIAB_ROOT/license}. The path to this folder is need stored in
environmental variable $WRSD\_LICENSE\_FILE$. It is used by the compiler to
determine is license is valid.

\par As testing application can be compiled from the command line (Command
Prompt in Windows), these environmental variable can be set via a \textbf{batch}
script. The official documentation for the Diab compiler provides instruction
how to generate all required variables. We provided the \textbf{env.bat} file as
an example.

\par However, Simulink doesn't have access to command line's environment.
Therefore, variables should be defined inside of the Matlab/Simulink
environment. We defined only licensing path in the
\path{PROJECT_ROOT/simulink/start.m}.

\par The \textbf{Makefile.config} contains a path to the folder that is used for store
compiled binaries and end firmware file. By default, this folder is
\path{PROJECT_ROOT/output} (the variable is \textit{OUTPUT\_PATH}). However,
the firmware generated by Simulink is stored in its project folder. The output
folder is used only for storing separate parts (e.g., Dio.o, OS\_alarm.o).

\par The assembled firmware is uploaded via P\&E Micro GDB server shipped in
the form of Eclipse plug-in for S32 Design Studio IDE. The Simulink's firmware is
uploading automatically via Makefile's target \textbf{upload}. The testing
application, in turn, should be uploaded with the following command \textbf{make
-f makefile upload}.

\par The uploading process is carried out by communication between P\&E Micro
GDB server and GNU PowerPC GDB. The \acrlong{gdb} is also shipped as part of S32
Design Studio. 

\begin{lstlisting}[
  style=makefile,
  caption=GDB script for firmware uploading,
  label={lst:flash_gdb},
  escapeinside={(*@}{@*)},
]
  target remote localhost:7224
  monitor _reset
  load firmware.elf
  detach
\end{lstlisting}

\par We automated \acrshort{gdb} uploading process via \textbf{*.gdb} script.
In case of the testing application, this script is hard-coded into \textbf{makefile}
under $\$(FLASH\_SCR)$ target. In case of Simulink projects, it is located in
\path{PROJECT_ROOT/simulink/autosar_pil_make_rtw_hook.m}. This is done due to
the fact that Makefiles generated by Simulink have different names depending on
the model's name. The \textit{\acrshort{rtw} hook} calls this Makefile with
\textbf{upload} target. After the first uploading, the GDB's script can be found
in the project folder. As it is standard \acrshort{gdb} debugging session, it is
possible to manually debug the target.

\par The listing \ref{lst:flash_gdb} shows the process of the firmware
uploading. First of all, PowerPC GDB [client] connects to the P\&E Micro GDB
server; the port can be changed in command line arguments we set it to 7224.
Then, debugger resets the target hardware and uploads firmware. Lastly, it
detaches from the GDB server and leaves firmware to execute on the hardware.

\par To assure that application is running it prints starting message on the
serial port. The message contains model's name and compilation datetime. The
example of the message is shown in the listing \ref{lst:first_msg}. The serial
link can be monitored via Putty (see section \ref{sec:make}).

\begin{lstlisting}[
  style=makefile,
  caption=Example of the model start message,
  label={lst:first_msg},
  escapeinside={(*@}{@*)},
]
===============Model Started===============
'model_name' - Wed Jun 19 14:10:44 2018 (TLC 8.3 (Jul 20 2012))
\end{lstlisting}

% $
%===============================================================================
%===============================================================================
\subsection{Simulink Folder Description} \label{sec:simulink_folder}

\begin{figure}[H]
  \begin{minipage}{\textwidth}
  \dirtree{%
.1 PROJECT\_ROOT/simulink.
  .2 autosar\_pil.tlc\DTcomment{System Target file}.
  .2 generate\_lib.m\DTcomment{Library assembling script}.
  .2 start.m\DTcomment{Entry point script}.
  .2 file\_process.tlc\DTcomment{\textit{mrmain}'s initialization template}.
  .2 slblocks.m\DTcomment{Block library control file}.
  .2 autosar\_pil\_make\_rtw\_hook.m\DTcomment{\acrshort{rtw} hooks}.
  .2 makefile.tmf\DTcomment{Makefile's template}.
  .2 sl\_customization.m\DTcomment{Customization of MATLAB env. in current folder}.
  .2 mrmain.tlc\DTcomment{Template of the main model's source file}.
  .2 compile\_blocks.m\DTcomment{Script for block compilation}.
  .2 autosar\_pil\_select\_callback\_handler.m\DTcomment{Callback for System Target file}.
  .2 rtiostream\_serial.c\DTcomment{Serial interface for External Mode support}.
  .2 setup.m\DTcomment{Library installation script}.
  .2 demos\DTcomment{Demonstration models}.
  .2 blocks\DTcomment{Library's blocks}.
    .3 header.c\DTcomment{S-Functions' common header}.
    .3 sfunction\_din.c\DTcomment{DIN S-Function}.
    .3 sfunction\_xxx.c\DTcomment{xxx S-Function}.
    .3 sfunction\_pwm.c\DTcomment{PWM S-Function}.
    .3 trailer.c\DTcomment{S-Functions' common trailer}.
    .3 din.slx\DTcomment{DIN visual model for Simulink}.
    .3 xxx.slx\DTcomment{xxx visual model for Simulink}.
    .3 pwm.slx\DTcomment{PWM visual model for Simulink}.
    .3 tlc\_c\DTcomment{TLC template for particular blocks}.
      .4 sfunction\_din.tlc\DTcomment{DIN TLC template}.
      .4 sfunction\_xxx.tlc\DTcomment{xxx TLC template}.
      .4 sfunction\_pwm.tlc\DTcomment{PWM TLC template}.
      .4 common.tlc\DTcomment{Common function for TLC templates}.
    }
  \end{minipage}
\caption{Shorten directory tree of the Simulink folder}
  \label{fig:sim_dirtree}
\end{figure}

\par Figure \ref{fig:sim_dirtree} shows the list of the files in Simulink folder.
The part of these scripts compiles and assemble the blocks library. The other
part sets and configures the environment. The following list describes purposes of
these files in more details:

\begin{itemize}
  \item \textbf{autosar\_pil.tlc} - is so-called System Target file. It exerts
    the process of code-generation during model build. It defines variables
    which are pivotal to the building process. For example, we defined only a few
    important parameters - the name Makefile's template, the name of
    code-generation utility (\textit{make\_rtw}), language, code format, target
    type, etc. For more information see \cite{stf}.
  \item \textbf{generate\_lib.m} - this script creates library file and adds
    blocks to it.
  \item \textbf{start.m} - it is an entry point to the library compilation. This
    file were created mainly to separate parameters which need to be defined
    before usage.
  \item \textbf{file\_process.tlc} - wrapper for \textit{mrmain.tlc} template.
    This file is used by \textit{autosar\_pil\_select\_callback\_handler.m} to
    set it as ERT custom template.
  \item \textbf{slblocks.m} - Simulink block library control file. It defines a
    few library's metadata for library browser.
  \item \textbf{autosar\_pil\_make\_rtw\_hook.m} - hook
    file\footnote{\url{https://www.mathworks.com/help/rtw/build-process-customization.html}}
    that invokes custom functions in the specific phases of the build process.
  \item \textbf{makefile.tmf} - template of the Makefile for the projects
    generated by Simulink.
  \item \textbf{sl\_customization.m} - the function which is called to customize
    Matlab's environment in the current folder\footnote{\url{https://www.mathworks.com/help/simulink/ug/registering-customizations.html}}.
  \item \textbf{mrmain.tlc} - template of the main model's source file. More
    details can be found in section \ref{sec:mrmain}.
  \item \textbf{compile\_blocks.m} - all blocks listed in the array in the
    \textit{start.m} script and which's corresponding files are presented in the
    \textit{blocks} folder are compiled via Matlab's MEX compiler.
  \item \textbf{autosar\_pil\_select\_callback\_handler.m} - callback function
    which called when \textit{autosar\_pil.tlc} is set as model's System Target
    file. This callback function sets recommended parameters of the model. These
    parameters can be changed via graphical user interface in model's
    preferences.
  \item \textbf{rtiostream\_serial.c} - abstraction layer between target serial
    link implementation and external mode libraries provided by MathWorks.
  \item \textbf{setup.m} - installation script which sets a few important
    variables. Then, it compiles blocks via \textit{compile\_blocks.m} and
    assembles them into library via \textit{generate\_lib.m}.
  \item \textbf{blocks/header.c} \& \textbf{blocks/trailer.c} - common header
    and trailer for blocks' sfunctions.
  \item \textbf{blocks/sfunction\_xxx.c} - sfunction for \textit{xxx} block.
    These files are just data sources/sinks which describe a number of block's
    inputs, outputs, parameters and carry a few additional operations such as
    parameters' validation.
  \item \textbf{blocks/xxx.slx} - visual representation of block \textit{xxx}.
    It is create a mask of the block with port labels and icon on the block. In
    other words, it is an appearance of the block in Simulink.
  \item \textbf{blocks/tlc\_c/common.tlc} - common header for TLC templates.
  \item \textbf{blocks/tlc\_c/sfunction\_xxx.tlc} - TLC template for
    \textit{xxx} block. This template is written in TLC language and used by
    Simulink to generate C code. Therefore, these files are code-description of
    the blocks.
\end{itemize}

\par The current library set contains 6 blocks: ADC, Digital Input (din),
Digital Output (dout), PWM, SCIR (SCI Receive), SCIS (SCI Sender), Overrun.
These are basic blocks with a minimal number of parameters. The Autosar MCAL
modules have a broad range of function to configure and interact with hardware
peripherals. The required functionality can be easily added to the existing
implementation.

\par The configuration of the Autosar system is a quite complicated process of
architecture planning and setting up all the components together. Moreover,
Autosar configuration is highly depended on project requirements and necessary
functionality (see section \ref{sec:autosar_os_conf}). Even though it is
possible to implement a simple Simulink wrapper for the provided interface, it
is time-consuming to handle all the situations of the driver utilization.
Simulink is not a configuring tool for Autosar, and it can not offer the same
level of functionality. Therefore, we agreed with the assignment submitter that
this set of blocks is enough for the demonstration of the system working
capacity and switched priority to the other parts of the project.

\begin{itemize}
  \item ADC - converts a value from a chosen channel
  \item Digital Input - reads boolean value (LOW/HIGH) on hardware pin
  \item Digital Output - writes boolean value (LOW/HIGH) to hardware pin
  \item PWM - sets PWM signal on a chosen channel with a variable duty cycle
  \item SCIR - receive one byte from the serial port 
  \item SCIS - sends one byte to the serial port
  \item Overrun - overrun detection block
\end{itemize}

\par The serial port is attached to the LIN2 hardware channel. The configuration
of this channel can be found in Autosar OS configuration. The parameters of the
serial link are hardcoded in the UART library located at
\path{PROJECT_ROOT/src/lib}. The parameters are 115200-8-N-1.

\par Note that we also added \textbf{overrun} diagnostic block to the library
(except visual model). Unfortunately, due to the fact that we failed to
integrate External mode support in our target, this block does not work as we
expected. It is a simple block with one boolean output which indicates an
occurrence of the overrun. However, as the target hardware can not communicate
with Simulink, it can not send an event that overrun occurred. Therefore, we change
it in the way that when overrun is detected the code corresponding to this block
sends message on a serial port.

\par The library can be assembled by running \textbf{start.m} script. It in turn
calls \textbf{compile\_blocks.m} and \textbf{generate\_lib.m}. The
\textbf{start.m} contains a few important parameters which need to be set before
a run. 

%===============================================================================
\section{User Application}

% \par The section \ref{sec:user_app} give a overview of the end application. 
\par We implemented single task -- multirate executing model. In this type of
architecture, all the particular model parts (with different sampling times)
created in Simulink are periodically executed in only one task. To decide which
part of the model execute and which not Simulink provides a mechanism in the
form of the function \textit{rate\_scheduler()}. This scheduler is executed with
fundamental model rate (a greatest common divisor of all sample times) and marks
all the control loops which need to be activated in this execution step.

\par The program consists of three task -- main periodic task (\textit{MAINTASK}),
simulation task (\textit{SIMULATIONTASK}) and external mode task
(\textit{EXTMODETASK}). All model related code is periodically called from the
simulation task. The main periodic task is responsible for overrun detection and
synchronization of the simulation task. The a mechanism for single tasking
architecture is shown in figure \ref{fig:single_tasking_model}.

\par The code of the user application is automatically generated from the model
developed via Simulink. There are two primary files which need to be mentioned -- 
\textit{ert\_main.c} and \textit{modelname.c} (filenames depend on the template
and model names, therefore, they can be different):

\subsection{ert\_main.c}\label{sec:mrmain}

\par \textbf{ert\_main.c} - it is the main entry point to the user application, and
it contains C \textit{main} function. This file is generated from a template
(\textit{mrmain.tlc}) which is written in the form of markup language used by
Simulink for code modification. \textit{mrmain.tlc} contains function calls and
variables definition which will be replaced by corresponding C code. 

\par The \textbf{ert\_main.c} is a file which defines application structure
in the form of tasks. It contains tasks functions and \textit{main()} function.
\textit{main()} is the initialization of basic modules (MCU, ECU), interrupts, serial
link and modules which added by Simulink. Simulink add only those modules which
it found in the model. For example, if the model uses ADC, then Simulink will add ADC
initialization into \textit{main()} function. This is done by the following
command: $\%<LibCallModelInitialize ()>$ (in \textit{mrmain.tlc}). In final
step $main()$ function pass control to the operating system.

\begin{figure}[H]
    \includegraphics[width=\textwidth]{images/app_inner_struct}
    \caption{Application inner structure}
    \centering
    \label{fig:single_tasking_model}
\end{figure}

\par There is three task function corresponding to each task -- MAINTASK,
EXTMODETASK, and SIMULATIONTASK. The MAINTASK is running with sample time equal
$500 \mu S$ via waiting for an event from the alarm. It is done for make
execution of the all task precise. This task runs on the highest required
frequency and starts SIMULATIONTASK with period calculated by Simulink. Also,
MAINTASK have highest priority and controls if SIMULATIONTASK overran or not.

\subsection{model.c}

\par \textbf{model.c} - this file is completly generated by Simulink. The file
is made up of three function -- initialization, one step of the model and
termination of the model. These functions are filled up with code in accordance
with model's utilization of the blocks. This means that every block has its 
initalization, step and termination procedures. Simulink Coder collects all
those procedures combine them and put into \textit{model.c}.

\par The most important part is \textit{one step} function. This function
implements control loop of the model. The custom blocks created in this project
will be described in accordance with their TLC templates.

%===============================================================================
%===============================================================================
\section{OS \& MCAL Configuration}\label{sec:os_config}

\par The OS is configured separately from particular MCAL modules. The textual
configuration files of the operating system can be found at
\textit{PROJECT\_ROOT/PIL-example/config}. It is stored in \textbf{*.xdm}
format which a proprietary format is providing enhanced usability features during
work with EB Tresos Studio \cite{as_training}. The following list contains main
points about OS configuration:

\begin{itemize}
  \item OS has one mode ("Mode01") and one application ("Application01")
  \item We configured three interrupts -- UART RX, UART TX and end of ADC conversion
  \item System has three tasks - MAINTASK, EXTMODETASK, and SIMULATIONTASK
    (described in section \ref{app_arch})
  \item OS has two counters. Both are attached to hardware clocks. First one is
    a system counter attached to a clock with a frequency of 80MHz. The second
    one is responsible for the timing of the tasks' execution, and frequency of its
    clock is 40MHz.
  \item The counter used for task activation is actually connected to the FXOSC
    via STM0 clock (see figure \ref{fig:stm0}) with prescaler which decrease
    input frequency to 2MHz
  \item The system has one alarm attached to the second 2MHz counter. This is
    cyclic alarm with period of 1000 ticks. At the overflow point it activates
    MAINTASK.
\end{itemize}

\par The more information about the configuration of the clock, counter, alarm
can be found in Appendix C (section \ref{sec:app_c}).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/STM0_Clock_generation}
    \caption{STM0 clock generation (taken from reference manual at \cite{mpc5748g})}
    \label{fig:stm0}
\end{figure}

\par Most of the MCAL modules are configured for its minimal working state. These
settings can be investigated by using project for EB Tresos Studio (see section
\ref{tresos_studio}) placed at \textit{PROJECT\_ROOT/Tresos Studio} or by
examining \textbf{*.epc} configuration files located at
\textit{PROJECT\_ROOT/generated/output}.

A few key configuration points which need to be mentioned:

\begin{itemize}
  \item ADC module is configured to convert a value from one channel (connected to
    a potentiometer)
  \item PWM module has only one channel connected to LED1 (DS1)
  \item One of the LIN channels is used for UART communication (LIN2)
  \item LED2 - 4 (DS2 - 4) are configured as outputs
\end{itemize}
