\chapter{PIL System Design}\label{ch:design}

\par In this Chapter, we will give the description of the software architecture
developed during this project. The system can be divided into two main parts.
First one is low-level embedded software made up of Operating System, MCAL
modules, Autosar configuration and user application. The second one is Simulink
related resources. It consists of the scripts exerting process of model code
generation, specific target configuration rules of a model and blocks' templates
composing Simulink library.

\par Firstly, we will give a brief description of the system architecture as
a total; which components made up the system and how they interact with each
other. Then we will describe each of them in more details.

%==============================================================================
\section{System Architecture}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/system_arch}
    \caption{System Architecture}
    \label{fig:system_arch}
\end{figure}

\par This section describes the architecture of the project and the way its
components interact with each other. Figure \ref{fig:system_arch} shows
a schematic diagram of the system arrangement and overview of the processes
happening in the system. The chart can be roughly divided into two parts. One
is statical and is not affected by Simulink (highlighted with a grey
rectangle). The second one is partially generated and mostly controlled by
Simulink.

\par Let's focus on the first part. By statical, we mean that these components are not
affected or modified during model development. The red blocks in the grey area
named "Autosar OS" and "MCAL" are the source code of Autosar Operating System and
MCAL modules accordingly. These sources are provided by NXP (for more
information see section \ref{sec:nxpswp}).

\par The left-side red block in the grey area is a configuration of Autosar OS
and MCAL modules. It is generated via EB Tresos Studio (see \ref{tresos_studio})
indicated as a green box. The IDE outputs files with many parameters used by
Autosar components to carry out hardware initialization, to define which
API is required and to create interface entities (e.g., channels, ports). The
last light-grey box on the right symbolize the Makefiles which defines the
key variables for the complation process.

\par The core component of the second part is Simulink. Simulink has a few
purposes. Firstly, it is used for model development. To create a model which
utilize microcontroller's peripherals you need to assemble and install a custom
library. The special script collects blocks found in a project folder compiles
them and then assemble them into the library. Next, the library is added in the
library browser and blocks can be freely used in the model.

\par The fact that model will be used for \acrshort{pil} simulation require
additional manipulations with model's configuration. The Matlab environment
allows to automate it via scripts. When the model is created user specify target
platform which defines all required parameters such as target language, Makefile
template, make utility, model solver, solver's parameters, etc.

\par The next purpose of the Simulink is to convert the model created by a user
into the source code. To accomplish this, we use the extension named Embedded Coder.
In addition to source code, it generates Makefile to compile and link all the
necessary files automatically.

\par The model is converted into the C source code by utilization of special
templates attached to every block. These templates describe how a block should
be integrated with the other parts, how it should be initialized and terminated,
and inner algorithm of the block \cite{emb_coder}. 

\par To summarise, before developing a new model Autosar components should be
brought to required configuration. That means that in this phase all the
peripherals, channels, system clocks, system tasks, etc. are created and
configured. After that, the whole process of the code generation, compilation
and uploading is controlled by Simulink. Embedded Coder generates source code of
the model using built-in blocks and blocks from the custom library. The code is
then compiled and uploaded to the target hardware. Finally, Simulink
indicates that process is successfully finished and the model is continue
executing on the board.

%==============================================================================
\section{Operating System Configuration}

\par The Autosar components have three different variants of configuration:
\textit{Pre-Compile}, \textit{Post-Build}, and \textit{Link-Time}
\cite{as_training}. In this project, we used two types -- \textit{PreCompile} and
\textit{PostBuild}. The first one means that all of the componenet's parameters
are generated in the form of source and header C files before the compilation of
the system and can not be changed in the run-time. The second one stores
multiple ECU configuration in the memory areas, and the system can switch between
them in run-time. Moreover, it allows to change some of the \acrshort{bsw}
properties and add other elements, but it is limited only to a specific set of
parameters \cite{vector_microsar}.

\subsection{Autosar OS Configuration}\label{sec:autosar_os_conf}

\par The Autosar operating system allows only pre-compile time type fo the
configuration parameters. It means that it is configured and scaled statically.
Therefore, most of the system parameters and functions such as alarms,
scheduling tables, number of tasks, stack sizes, interrupts are configured
before compilation and can not be changed in a run-time. 

\subsection{MCAL modules Configuration}\label{sec:mcal_conf}

\par A microcontroller consists of many hardware components such as clocks, CPU
cores, communication modules, etc. Before running any operating system or user
application, these peripherals need to be configured. Autosar MCAL is
microcontroller abstraction layer for these hardware components.  

\par Even though Autosar is a highly flexible system, it is accomplished by
limiting the system configuration making it less dynamic. For example, it is
impossible to change pin mode from a digital output to PWM-driven one (NXP
safety implementation does not support this API). The API offered by the
operating system also relies on the configuration and need to be enabled or
disabled depending on the project requirements. Moreover, some of the parameters
are strictly limited by Autosar specification and can have only
\textit{Pre-Compile} type of configuration.

\par NXP's MCAL modules are distributed in the form of plug-ins for EB Tresos
Studio (see \ref{tresos_studio}). Plug-in folder includes templates in the form
of Tresos Studio markup language. These templates are used to generate C
language headers and source code files which contains the configuration of
individual MCAL modules.

\par Plug-in folder also contains a statical implementation of MCAL module. In
other words, it implements all structures and functions which provides an
interface for interaction with hardware. This implementation is also need to be
included in the project.

%===============================================================================
%===============================================================================
\section{The use of \acrshort{rte}}

\par \acrshort{autosar} is a widely used system. Nowadays most of the car parts
have their built-in microcontroller for monitoring, communication, and
control. Most of the \acrshort{oem}s do not produce the whole set of the parts
required to assemble a car. They focus on one specific set of products and
supply it to bigger companies \cite{vectoracademy}. It becomes important to
offer compatible software for interact and control these parts. The primary
purpose of Autosar is to standardize this software via abstraction layers. In
Autosar ideology these applications should be placed at the application layer
(top layer in figure \ref{autosar_img_nxp}).

\begin{figure}[H]
    \includegraphics[width=\textwidth]{images/nxp_autosar}
    \caption{Autosar implementation provided by NXP \cite{mpc5748g}}
    \centering
    \label{autosar_img_nxp}
\end{figure}

\par In an Autosar system, the user application is called \acrlong{swc}s. These
are located in the application layer of Autosar architecture. They can
communicate with each other and with operating system services via
\acrlong{rte}. \acrshort{rte} with other \acrlong{bsw} implements \acrlong{vfb}
concept \cite{rte_spec}. It is a pivotal idea in Autosar architecture that
assures standardized communication between components. That, in turn, allows
\acrshort{oem}s to ship the same controlling application for different variation
of hardware configuration.

\par Simulink software already has tools for validation, development, and
simulation of the software components \cite{sim_autosar}. Unfortunately,
implementation of the Autosar provided by NXP does not implement \textbf{I/O
Hardware Abstraction} basic software component (see figure \ref{autosar_img_nxp}).
It is an interface between microcontroller abstraction layer and \acrshort{rte} 
\cite{rte_spec}. 

\par Even though it is not the part of the problem what we are solving, it would
bring some model portability and reliability of the MathWorks tools. As a result,
we couldn't use \acrshort{vfb} for data acquisition and control over peripheral
modules. Instead, we used interface provided by \acrshort{mcal} layer to interact
with on-chip modules.

%==============================================================================
%==============================================================================
\section{User Application}\label{sec:user_app}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.65\textwidth]{images/pil_firmware_structure}
    \caption{Simulink generates only user application (model's code)}
    \label{pil_firmware}
\end{figure}

\par The user application is meant to be a software piece which utilizes the
operating system for its custom behavior. In our case, it represents software
generated from a developed model. As we do not use \acrshort{rte} and can not
utilize its interface this application can not be placed at the application
layer of Autosar layer structure (see \ref{autosar_structure}). We can assume
that our application is above MCAL abstraction layer with tight integration with
system services.

\par The structure of the application is fixed and have a skeleton which later
is filled up with code generated from a control loop of the Simulink model. The
skeleton already contains integration with Autosar OS and a few important MCAL
modules initialization. It defines all the tasks and their inner structure.

\subsection{Simulink Skeleton of the application}

\par The skeleton is not a regular C language file; it is template written in
special TLC language. This template is parsed by Simulink and then filled up
with necessary code constructions. Code which will be placed in the template
depends on many parameters defined by Simulink. For example, although template
has a construction which needs to be replaced by code for external mode support
if this support is disabled in model settings (in Simulink), it will not be
generated.

\par The model's control loop is placed in a separate file completly generated
by Simulink. It mainly consists of three functions which are called from the main
application file. These functions are responsible for initialization, one
control loop step, and termination of the model. The control loop of the model
is placed under \textit{one step} (\textit{modelname\_step(void)} in the code)
function.  The
\textit{initialization} (\textit{modelname\_initialize()} in the code) function
contains intial source code of the blocks.  Every block template describes the 
process of the block intialization and termination. The last function \textit{termination} is
similar to \textit{initialization} function, but it is called at the model
termination stage.

\par The generated control loop is made up of parts of the code corresponding to
separate blocks. These code pieces interact via code structure generated on the
basis of the blocks' description (e.g., number of the inputs/outputs, types of the
inputs). Simulink knowns the way to handle its built-in blocks. We
implemented similar templates for our custom blocks. These blocks TLC templates
were based on RPP project implementation. More information about blocks
implementation can be found at \cite{rpp}, \cite{tlc}, \cite{sfunction_basics}
and \cite{c_sfunction}.

\subsection{Architecture}\label{app_arch}

\par The application architecture has a few primary criteria which need to be
satisfied: application unable to change OS parameters, execution rate need to be
precise, minimal execution rate of the model can vary, and application should
detect control loop overruns.

\par As mentioned earlier Autosar OS is statically configured and can not be
changed in run-time, the application should be structured in the way that it does
not require any OS configurations. For example, we can not manipulate with
task's priority or length of the alarms' cycle to obtain required task execution
behavior.

\par Minimally required execution rate of the model is $500\mu S$. We used it as a
fundamental frequency for the whole application. It is accomplished by a hardware
counter and alrarm attached to it. The alarm is system object which can be
configured to activate specific task every \textbf{n} ticks. The alarm is
attached to the counter which is incremented by hardware clock (FXOSC)
configured to specific frequency, in our case $40 MHz$. Between the hardware
clock and the software counter there is clock STM0 which inputs is connected to
FXOSC and output is configured as input for the counter. It has prescaler
which decrease count frequency to $2 MHz$. The alarm's period length is 1000
ticks. Therefore, every $500 \mu S$ alarm activates a task which is responsible
for the control of the application. This task also has the highest priority.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/tasks_structure}
    \caption{Internal structure of the user application}
    \label{tasks_structure}
\end{figure}

\par This periodic task (in implementation it is named MAINTASK) has two main
purposes -- to carry overrun diagnostics and activate model task responsible for
model execution. Main and model tasks are synchronized via system events
(similar to semaphores). Model task unlimitedly waits for an event to occur.
Only the periodic task decide when event need to be set. 

\par Model task need have to be executed with fundamental rate defined by Simulink.
This rate is calculated as a greatest common divisor of all sample rates of all
control loops presented in the model. The main periodic task, mentioned earlier,
uses this rate to calculate period at which execution cycle it needs to activate
the model task. For example, if the fundamental rate is 0.05 and the main task
always has rate 0.0005, then, the main task will set event every 100 cycles.

\par When the model has multiple control loops with different sample rates they are
handled as follow:

\begin{enumerate}
  \item Simulink calculates fundamental rate -- the smallest step required to all
    loops were in phase
  \item Main periodic task executes the model task with this smallest step
  \item Function generated by Simulink marks parts of the model which need to be
    executed in this step
  \item Parts which should not be executed are skipped
\end{enumerate}

\par In this type of architecture called singletask -- multirate overrun
detection reduces to control of a single task. As main periodic task has the
highest priority, it can preempt execution from the model task and check if it
is within time limits or not.

\par There also exists a third task named EXTMODETASK which is used for External
Mode support. This task is filled up with code generated by Simulink Coder.

%==============================================================================
%==============================================================================
\section{Simulink Library}

\par We created a custom Simulink library with blocks which represent an
interface to hardware peripherals. In other words, these blocks are the visual
representation of the code which will be placed in the final sources.

\par Visual models of the blocks are attached to templates. These templates
contain the description of C-code construction for block initialization,
execution, and termination.

\par More information about the process of creating a custom library and the way
libraries works can be found at \cite{sim_libs}.

\subsection{Block internal structure}\label{sec:blocks_struct}

\par S-function is a Simulink interface which allows using custom blocks in
Simulink models. In order to utilize it, we need to define a few block's
attributes which will determine the way block will be function and presented in
Simulink. 

\par The particular blocks are defined by three files -- visual model, \acrlong{mex}
(\acrshort{mex}) and TLC template. The visual model is built-in Simulink
block attached to \acrshort{mex} file. It can have a mask manually created by
a developer with port labels, parameters help, an icon on the block, etc. to
make the block more recognizable. The \acrlong{mex} file is written in a
compilable language (in our case it is C) and presents executable part of the
block. The Simulink calls function from \acrshort{mex} file to obtain block
attributes such as the number of inputs, number of outputs, number of
parameters, etc. The C \acrshort{mex} files are designed to be flexible in its
implementation; it can be, for example, complex algorithm. In this project, we
used it as the description of block's attributes (number of
inputs/outputs/parameters). The last file is TLC template. It is
code-representation of the block. These files are written in TLC language. These
templates are not used in model development, only in code-generation phase. For
more information about block structure see \cite{c_sfunction},
\cite{c_mex_sfunction} and \cite{rpp}.

%==============================================================================
%==============================================================================
\section{PIL simulation flow}

\par The application can be roughly divided into two parts -- OS and User
Application (figure \ref{pil_firmware}). As the OS does not require any
modification, Simulink will treat it as user's additional sources.  We can
automate the OS compilation process by modifying Makefile generated for model
compilation.

\par Makefile is created from a template which is parsed and filled up with
model's parameters. This template includes Makefiles for OS compilation. Also,
these Makefiles specifies compiler, building flags, output folder, rules for
other extra sources (e.g., UART), etc. In result, we get a classical C project
which can be compiled even without Simulink by using building control tool (for
example \textbf{make} utility; see \ref{sec:make}). 

\par The user application (model) is transformed into code. The source code of
the blocks is based on TLC templates. Simulink uses code structures based on
blocks description provided in TLC and s-function templates to interconnect
blocks inside of the application.

\par The whole PIL simulation workflow including code-generation and compilation
can be seen in figure \ref{pil_execution}. More information about \acrshort{pil}
simulation can be found at \cite{pil_raspb} and \cite{emb_coder}.

%==============================================================================
%==============================================================================
\section{Run-time Model behavior}

\par In a run-time Simulink can connect to the target hardware via a serial link
or TCP/IP. If the connection is successfully set up, the user can change blocks
parameters and view dataflow of the model, like in software simulation. This is
done by step-nature of the model execution. Communication between Simulink and
target hardware is based on server-client architecture where target is server
and Simulink is client. Unfortinately, there is no official description of the
protocol used for communication.

\par The Mathworks, Inc. provides library written in C language which need to be
included into the target's firmware. This library implements communication
protocol and handling of the model modifications.

% \par Simulink Coder manage project at the level of compilation. It generates and
% then executes Makefiles. We created custom Makefile template to include OS and
% MCAL modules compilation and utilization of custom compilator.  Also, we add
% functionality to upload final firmware straight into target hardware.
