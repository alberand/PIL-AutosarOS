### External Mode

**Difference between PIL and External Mode (Simulink)?**

From official MathWorks documentation:

In this example you will learn how to configure a Simulink model to run
Processor-In-the-Loop (PIL) and External Mode simulations. In a PIL simulation,
the generated code runs on target hardware. The results of the PIL simulation
are transferred to Simulink to verify the numerical equivalence of the
simulation and the code generation results. The PIL verification process is a
crucial part of the design cycle to ensure that the behavior of the deployment
code matches the design.

Simulink's External mode feature enables you to accelerate the process of
parameter tuning by letting you change certain parameter values while the model
is running on target hardware, without stopping the model. When you change
parameter values from within Simulink, the modified parameter values are
communicated to the target hardware. The effects of the parameter tuning
activity may be monitored by viewing algorithm signals on scopes or displays in
Simulink.

### How to integrate External mode into custom target?

The file __rtiostream_interface.c__ is an interface between the external mode 
protocol and an rtiostream communications channel

Implementing External mode for custom target:
https://www.mathworks.com/help/rtw/ug/creating-a-tcp-ip-transport-layer-for-external-communication.html

How to implement rtiostream transport channel (lowest HW layer):
https://www.mathworks.com/help/ecoder/ug/target-connectivity-pil-api-components.html#bru4krc-1

There is good example of usage of External mode (in target):
MATLAB\R2016b\rtw\c\src\common\rt_main.c

### The problem

When Simulink wants to connect to Target it sends:
```
	HEX:   7E7E01080000006578742D6D6F64650303
	ASCII: ~~<SOH><BS><NUL><NUL><NUL>ext-mode<ETX><ETX>
```
and waits for response. But target doesn't return anything and wait for more
data. To be more precise at the begining it waits for 8 bytes; when it receive them
it waits for 1 byte. Even if it gets one more byte it start to wait for the next
one.

### Files

The files shipped in this folder are Wildshark recording of communication
between Simulink and Linux Target for Simulink Embedded coder (see [1] and [2]).
However, this communication were carried out by TCP/IP suite; not a serial link.
The *.txt document contains extracted packages infromation:
```
    package number, port of the sender, hex data, ascii data
```

[1]: http://rtime.felk.cvut.cz/~sojka/blog/on-generating-linux-applications-from-simulink/
[2]: http://lintarget.sourceforge.net/
