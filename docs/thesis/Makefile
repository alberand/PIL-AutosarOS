.PHONY: all fast clean clean-images clean-aux help

# TODO 
# * Add draftmode (see man pdflatex -> draftmode)
# * Add graphics generating from data (python scripts)

DOC	 		= thesis.pdf
FINAL_NAME  = DP_2018_Andrey_Albershteyn.pdf

TEX  		= template.tex
LATEX  		= pdflatex
LATEXFLAGS 	= -pdf # -aux-directory=./tmp
BIB  		= bibtex
BIBFLAGS	=

TEXT_DIR 	= ./sections
TEXT_SRCS  	= $(wildcard $(TEXT_DIR)/*.tex)
GRAPHIC_DIR = ./images
FIGSRC		= $(wildcard $(GRAPHIC_DIR)/*.svg)
FIG			= $(FIGSRC:%.svg=%.pdf)
FORMATS		= aux log blg bbl lof out bcf fls idx ind lof fdb_latexmk acn \
			  acr alg glg glo gls glsdefs ist

VIEWER 		= zathura

all: $(DOC)

$(DOC): $(TEX) $(FIG)
	$(LATEX) $(LATEXFLAGS) $(TEX)
	$(BIB) $(BIBFLAGS) $(TEX:%.tex=%)
	makeglossaries $(TEX:%.tex=%)
	$(LATEX) $(LATEXFLAGS) $(TEX)
	mv $(TEX:%.tex=%.pdf) $(DOC)
	$(MAKE) rename
	# $(MAKE) clean-aux

$(GRAPHIC_DIR)/%.pdf: $(GRAPHIC_DIR)/%.svg
	inkscape --export-pdf=$@ --export-area-page $<
	
examine: ## Examine duplicates, passive, and weasel words occurence
	dups $(TEXT_SRCS) || true
	passive $(TEXT_SRCS) || true
	weasel $(TEXT_SRCS) || true

show:
	$(VIEWER) $(FINAL_NAME)

rename:
	mv $(DOC) $(FINAL_NAME)

clean-pdf:
	rm -f $(DOC)

clean-aux: ## Remove helping files generated while compilation via pdflatex
	# Here can be used 'latexmk -c'
	rm -f $(foreach format,$(FORMATS),$(wildcard *.$(format)))
	rm -f $(foreach format,$(FORMATS),$(wildcard $(TEXT_DIR)/*.$(format)))

clean-images:
	rm -f $(FIG)

clean: clean-images clean-aux
	rm -f $(DOC)

help:  # http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
