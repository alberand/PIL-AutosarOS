function blkStruct = slblocks

% Copyright (C) 2013 Czech Technical University in Prague
%
% Authors:
%     - Carlos Jenkins <carlos@jenkins.co.cr>
%
% This document contains proprietary information belonging to Czech
% Technical University in Prague. Passing on and copying of this
% document, and communication of its contents is not permitted
% without prior written authorization.
%
% File : slblocks.m
% Abstract:
%     Simulink block library control file.
%
% References:
%     rtw_ug.pdf p. 1127
%     Example in <matlabroot>/toolbox/simulink/blocks/slblocks.m
%

    Browser.Library = 'pil_mpc5748g';
    Browser.Name    = 'Autosar PIL - MPC5748G';
    blkStruct.Browser  = Browser;
