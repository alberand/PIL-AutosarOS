/*
 * Copyright 1994-2012 The MathWorks, Inc.
 *
 * Modified for RPP target by Michal Sojka <sojkam1@fel.cvut.cz>
 *
 * File: rtiostream_serial.c
 *
 * Abstract:
 *  Serial communications channel for a Windows/UNIX machine. RPP specific support for
 *  Simulink external mode (over serial line).
 */

#include "rtiostream.h"
#include "uart.h"

#define READ_FILE_TIMEOUT 10000         /* 10 seconds */
#define WRITE_FILE_TIMEOUT 2000         /* 2 seconds. This should be large
                                         * enough to send 512 bytes at the
                                         * lowest possible baud rate. See
                                         * comments in serialDataSet for
                                         * more details
                                         */

/***************** VISIBLE FUNCTIONS ******************************************/

/* Function: rtIOStreamOpen =================================================
 * Abstract:
 *  Open the connection with the target.
 */
int rtIOStreamOpen(int argc, void * argv[])
{
    return RTIOSTREAM_NO_ERROR;
}



/* Function: rtIOStreamSend =====================================================
 * Abstract:
 *  Sends the specified number of bytes on the comm line. Returns the number of
 *  bytes sent (if successful) or a negative value if an error occurred. As long
 *  as an error does not occur, this function is guaranteed to set the requested
 *  number of bytes; the function blocks if the TCP/IP send buffer doesn't have
 *  room for all of the data to be sent
 */
int rtIOStreamSend(
    int streamID,
    const void * const src,
    size_t size,
    size_t *sizeSent)
{
    if(uart_send((uint8_t *)src, size) == 1){
        *sizeSent = size;
        return RTIOSTREAM_NO_ERROR;
    }  
    else
    {
        // Never happens
        return RTIOSTREAM_ERROR;
    }  
    
}


/* Function: rtIOStreamRecv ================================================
 * Abstract:
 *  Attempts to gets the specified number of bytes from the specified serial.
 *  The number of bytes read is returned via the 'sizeRecvd' parameter.
 *  RTIOSTREAM_NO_ERROR is returned on success, RTIOSTREAM_ERROR is returned on
 *  failure.
 *
 * NOTES:
 *  o it is not an error for 'sizeRecvd' to be returned as 0
 *  o this function waits for at most READ_FILE_TIMEOUT
 */
int rtIOStreamRecv(
    int      streamID,
    void   * const dst,
    size_t   size,
    size_t * sizeRecvd)
{
    CONSOLE_MESSAGE("rtIOStreamRecv %d b\n", size);
    if (uart_recv((uint8_t*)dst, size) == 1) {
        *sizeRecvd = size;
        return RTIOSTREAM_NO_ERROR;
    } else{
        return RTIOSTREAM_ERROR;
    }
}


/* Function: rtIOStreamClose ================================================
 * Abstract: close the connection.
 *
 */
int rtIOStreamClose(int streamID)
{
    return RTIOSTREAM_NO_ERROR;
}