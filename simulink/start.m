% Initialization file

% This demo is opened in the last step 
demo_name = 'io_demo';

% Wind River Diab compiler root
compFolder  = 'C:\Apps\Diab';
% License folder for Diab
compLicense = strcat(compFolder, '\license');
% Folder with windows binaries
compRoot    = strcat(compFolder, '\compilers\diab-5.9.6.2\WIN32');

p = pwd();
inds = strfind(pwd(), filesep);
% Project root
projectRoot = p(1:inds(end)-1);

%==========================================================================
% Generate lib
%==========================================================================
% Blocks to compile and then add to the lib
blocks = {
	'din',      % Digital input
	'dout',     % Digital output
    'or',
    'adc',
    'pwm',
    'scis',     % SCI send
    'scir',     % SCI receive
};

disp('Start library generation process');
setup(blocks, compRoot, projectRoot);
disp('Load library');
load_system('blocks/pil_mpc5748g.slx');

%==========================================================================
% Open model and set params
%==========================================================================

% Create env. var with Wind River Diab compiler license
setenv('WRSD_LICENSE_FILE', compLicense);

% Open demo
open_system(strcat('demos/', demo_name, '.slx'));
