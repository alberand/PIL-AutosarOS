% Copyright (C) 2013, 2014, 2015 Czech Technical University in Prague
%
% Authors:
%     - Carlos Jenkins <carlos@jenkins.co.cr>
% Modified:
%     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
%
% This document contains proprietary information belonging to Czech
% Technical University in Prague. Passing on and copying of this
% document, and communication of its contents is not permitted
% without prior written authorization.
%
% File : rpp_setup.m
% Abstract:
%     Target install script.
%
%         1. Start Matlab.
%         2. Change directory (cd) to this file folder.
%         3. Execute setup on command window.
%
% References:
%     rtw_ug.pdf p. 1137

function setup(blocks, compRoot, projectRoot)
    % Get current target path in user's filesystem
    currentPath = pwd();
    targetPath = currentPath;

    CompilerRoot = fix_slash(compRoot);
    TargetRoot   = fix_slash(currentPath);
    projectRoot  = fix_slash(projectRoot);

    if ispc, ext='.exe'; else ext=''; end
    armcl = fullfile(CompilerRoot, 'bin', ['dcc' ext]);
    %if ~exist(armcl, 'file'),
        %fprintf('Error: "%s" does not exist!\n', armcl);
        %return;
    %end

    % Remove old paths and preferences (if any)
    warning('off', 'MATLAB:rmpath:DirNotFound')
    if ispref('rpp')
        oldTargetPath = getpref('rpp', 'TargetRoot');
        rmpath(fullfile(oldTargetPath, '..', 'rpp'));
        rmpath(fullfile(oldTargetPath, '..', 'blocks'));
        rmpref('rpp');
    end
    warning('on', 'MATLAB:rmpath:DirNotFound')

    % Add target system folders to Matlab path
    addpath(fullfile(targetPath, 'blocks'));
    savepath();

    % Save preferences
    addpref('rpp', 'CompilerRoot', CompilerRoot);
    addpref('rpp', 'TargetRoot'  , TargetRoot);
    addpref('rpp', 'ProjectRoot'  , projectRoot);


    % Generate blocks and library
    addpath(pwd());
    cd('blocks');
    disp('Compiling blocks...');
    compile_blocks();       % sf.c -> sf.mex
    generate_lib(blocks);     % touch rpp_lib.slx
    cd(currentPath);
    rmpath(pwd());

    % Apply changes and finish
    sl_refresh_customizations();
    disp('Target setup is complete!');
    disp('    Current configuration:');
    disp(['        CompilerRoot : ', CompilerRoot]);
    disp(['        TargetRoot   : ', TargetRoot]);
    disp(['        Hardware     : ', 'mpc5748g']);
end


function path = fix_slash(path0)
    path = path0;
    if ispc
        path(path=='\')='/';
    end
end
