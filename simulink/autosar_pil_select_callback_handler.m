function autosar_pil_select_callback_handler(hDlg, hSrc)
    % The target is model reference compliant
    slConfigUISetVal(hDlg, hSrc, 'ModelReferenceCompliant', 'on');
    slConfigUISetEnabled(hDlg, hSrc, 'ModelReferenceCompliant', false);

    % Set the target language to C and disable modification
    slConfigUISetVal(hDlg, hSrc, 'TargetLang', 'C');
    slConfigUISetEnabled(hDlg, hSrc, 'TargetLang', false);

    % Disable Report
    slConfigUISetVal(hDlg, hSrc, 'GenerateReport', false);
    slConfigUISetVal(hDlg, hSrc, 'LaunchReport', false);

    % Set code generation templates and params
    % slConfigUISetVal(hDlg, hSrc, 'SystemTargetFile', 'autosar_pil.tlc');

    slConfigUISetVal(hDlg, hSrc, 'GenerateMakefile', 'on');
    slConfigUISetVal(hDlg, hSrc, 'TemplateMakefile', 'makefile.tmf');
    slConfigUISetVal(hDlg, hSrc, 'MakeCommand', 'make_rtw');

    % Configure solver
    slConfigUISetVal(hDlg, hSrc, 'SolverType', 'Fixed-step');
    slConfigUISetVal(hDlg, hSrc, 'Solver', 'FixedStepDiscrete');
    slConfigUISetVal(hDlg, hSrc, 'FixedStep', 'auto');
    slConfigUISetVal(hDlg, hSrc, 'StopTime', 'inf');

    % Hardware being used is the production hardware
    slConfigUISetVal(hDlg, hSrc, 'ProdEqTarget', 'on');

    % For real-time builds, we must generate ert_main.c
    slConfigUISetVal(hDlg, hSrc, 'ERTCustomFileTemplate', 'file_process.tlc');
    slConfigUISetVal(hDlg, hSrc, 'GenerateSampleERTMain', 'off');
    slConfigUISetEnabled(hDlg, hSrc, 'GenerateSampleERTMain', true);

    % Set the TargetLibSuffix
    slConfigUISetVal(hDlg, hSrc, 'TargetLibSuffix', '.a');

    % Interface
    slConfigUISetVal(hDlg, hSrc, 'ExtModeMexArgs', ['1 ''COM4'' 115200']);
end