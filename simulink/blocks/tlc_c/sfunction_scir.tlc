%% Copyright (C) 2013-2014 Czech Technical University in Prague
%%
%% Authors:
%%     - Carlos Jenkins <carlos@jenkins.co.cr>
%%
%% This document contains proprietary information belonging to Czech
%% Technical University in Prague. Passing on and copying of this
%% document, and communication of its contents is not permitted
%% without prior written authorization.
%%
%% File : sfunction_scir.tlc
%% Abstract:
%%     TLC file for inlining RPP serial communication interface receive block.
%%
%% References:
%%     BlockTypeSetup() : refs/rtw_tlc.pdf p. 277
%%     Start()          : refs/rtw_tlc.pdf p. 279
%%     Outputs()        : refs/rtw_tlc.pdf p. 281


%implements sfunction_scir "C"

%include "common.tlc"


%% Function: BlockTypeSetup ====================================================
%function BlockTypeSetup(block, system) void

    %% Ensure required header files are included
    %<RppCommonBlockTypeSetup(block, system)>
    %<LibAddToCommonIncludes("uart.h")>

%endfunction


%% Function: Start =============================================================
%function Start(block, system) Output

    %if !SLibCodeGenForSim()
        %%No initialization needed for this block
    %endif

%endfunction


%% Function: Outputs ===========================================================
%function Outputs(block, system) Output

    %if !SLibCodeGenForSim()

        %if extMode
	    %return
	    %endif

        // Quick fix for var declaration (can't declare in the mid. of func.)
        {
            %% Declare temporal variables
            %if EXISTS("_APIL_SCIR_TMP_VARS_") == 0
                %assign ::_APIL_SCIR_TMP_VARS_ = 1
                uint8_t scir_tmp;
            %endif

            %% Get IO signals
            %assign data     = LibBlockOutputSignal(0, "", "", 0)
            %assign err_flag = LibBlockOutputSignal(1, "", "", 0)

            scir_tmp = sci_a_getchar();
            if (scir_tmp == 0) {
                %<data> = -1;
                %<err_flag> = TRUE;
            } else {
                %<data> = (uint8_t)scir_tmp;
                %<err_flag> = FALSE;
            }
        }

    %endif

%endfunction

%% [EOF]
