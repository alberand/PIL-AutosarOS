/* Copyright (C) 2013, 2014 Czech Technical University in Prague
 *
 * Authors:
 *     - Carlos Jenkins <carlos@jenkins.co.cr>
 * Modified:
 *     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
 *
 * This document contains proprietary information belonging to Czech
 * Technical University in Prague. Passing on and copying of this
 * document, and communication of its contents is not permitted
 * without prior written authorization.
 *
 * File : sfunction_adc.c
 * Abstract:
 *     C-MEX S-function block for ADC module.
 *
 * References:
 *     header.c
 *     trailer.c
 *
 * Compile with:
 *     <matlabroot>/bin/mex sfunction_adc.c
 */

/*
%YAML 1.2
---
Name: ADC
Category: IO blocks
Mnemonic: ADC

Inputs:

Outputs:
  - { name: "ADC Input", type: "bool" }

Parameters:
  - { name: "Pin number",             type: "uint16", range: "[0-65536]"           }

# Description and Help is in Markdown mark-up
Description: |

  Gets the digital value of the specified digital input pin on the board.

Help: |

  ADC input
...
*/

#define S_FUNCTION_NAME sfunction_adc
#include "header.c"


static void mdlInitializeSizes(SimStruct *S)
{
    /*
     * Configure parameters: 1
     *  - Pin number
     */
    if (!rppSetNumParams(S, 1)) {
        return;
    }

    /*
     * Configure output ports: 1
     *  - Digital input.

     */
    if (!ssSetNumOutputPorts(S, 1)) {
        return;
    }
    rppAddOutputPort(S, 0, SS_UINT16);

    /* Set standard options for this block */
    rppSetStandardOptions(S);
}



#ifdef MATLAB_MEX_FILE
#define MDL_SET_WORK_WIDTHS
static void mdlSetWorkWidths(SimStruct *S)
{
    /* Set number of run-time parameters */
    if (!ssSetNumRunTimeParams(S, 1)) {
        return;
    }

    /* Register the run-time parameter 1 */
    ssRegDlgParamAsRunTimeParam(S, 0, 0, "p1", SS_INT16);
}
#endif

#define UNUSED_MDLCHECKPARAMETERS
#define COMMON_MDLINITIALIZESAMPLETIMES_INHERIT
#define UNUSED_MDLOUTPUTS
#define UNUSED_MDLTERMINATE
#include "trailer.c"
