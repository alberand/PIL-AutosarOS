/* Copyright (C) 2013, 2014 Czech Technical University in Prague
 *
 * Authors:
 *     - Carlos Jenkins <carlos@jenkins.co.cr>
 * Modified:
 *     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
 *
 * This document contains proprietary information belonging to Czech
 * Technical University in Prague. Passing on and copying of this
 * document, and communication of its contents is not permitted
 * without prior written authorization.
 *
 * File : sfunction_lout.c
 * Abstract:
 *     C-MEX S-function block for RPP digital output.
 *
 * References:
 *     header.c
 *     trailer.c
 *
 * Compile with:
 *     <matlabroot>/bin/mex sfunction_lout.c
 */

/*
%YAML 1.2
---
Name: Digital Output
Category: IO blocks
Mnemonic: LOUT

Inputs:
  - { name: "Digital Output",   type: "bool"  }

Parameters:
  - { name: "Pin number", type: "uint16"}

# Description and Help is in Markdown mark-up
Description: |

  Sends the digital value to the specified logic output pin on the board.

Help: |

  Digital output
...
*/

#define S_FUNCTION_NAME sfunction_lout
#include "header.c"


static void mdlInitializeSizes(SimStruct *S)
{
    /*
     * Configure parameters: 1
     *  - Pin number
     */
    if (!rppSetNumParams(S, 1)) {
        return;
    }

    /*
     * Configure input ports: 1
     *  - Digital output.
     */
    if (!ssSetNumInputPorts(S, 1)) {
        return;
    }
    rppAddInputPort(S, 0, SS_BOOLEAN);
    
    /*
     * Configure output ports: 0
     */
    if (!ssSetNumOutputPorts(S, 0)) {
        return;
    }

    /* Set standard options for this block */
    rppSetStandardOptions(S);
}

#ifdef MATLAB_MEX_FILE
#define MDL_SET_WORK_WIDTHS
static void mdlSetWorkWidths(SimStruct *S)
{
    /* Set number of run-time parameters */
    if (!ssSetNumRunTimeParams(S, 1)) {
        return;
    }

    /* Register the run-time parameter 1 */
    ssRegDlgParamAsRunTimeParam(S, 0, 0, "p1", SS_UINT16);
}
#endif

#define UNUSED_MDLCHECKPARAMETERS
#define COMMON_MDLINITIALIZESAMPLETIMES_INHERIT
#define UNUSED_MDLOUTPUTS
#define UNUSED_MDLTERMINATE
#include "trailer.c"
