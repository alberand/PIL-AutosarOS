/* Copyright (C) 2013, 2014 Czech Technical University in Prague
 *
 * Authors:
 *     - Carlos Jenkins <carlos@jenkins.co.cr>
 * Modified:
 *     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
 *
 * This document contains proprietary information belonging to Czech
 * Technical University in Prague. Passing on and copying of this
 * document, and communication of its contents is not permitted
 * without prior written authorization.
 *
 * File : sfunction_scir.c
 * Abstract:
 *     C-MEX S-function block for Serial Communication receive.
 *
 * References:
 *     header.c
 *     trailer.c
 *
 * Compile with:
 *     <matlabroot>/bin/mex sfunction_scir.c
 */

/*
%YAML 1.2
---
Name: Serial Receive
Category: Communication
Mnemonic: SCIR

Inputs:

Outputs:
  - { name: "Data", type: "uint8" }

Parameters:

# Description and Help is in Markdown mark-up
Description: &desc |

  This block receives a byte from the SCI.

Help: *desc

Status: Stable
...
*/


#define S_FUNCTION_NAME sfunction_scir
#include "header.c"


static void mdlInitializeSizes(SimStruct *S)
{
    /*
     * Configure parameters: 0
     */
    if (!rppSetNumParams(S, 0)) {
        return;
    }

    /*
     * Configure input ports: 0
     */
    if (!ssSetNumInputPorts(S, 0)) {
        return;
    }

    /*
     * Configure output ports: 1
     *  - Data receive.
     *  - Err flag
     */
    if (!ssSetNumOutputPorts(S, 2)) {
        return;
    }
    rppAddOutputPort(S, 0, SS_UINT8);
    rppAddOutputPort(S, 1, SS_BOOLEAN);

    /* Set standard options for this block */
    rppSetStandardOptions(S);
}


#define COMMON_MDLINITIALIZESAMPLETIMES_INHERIT
#define UNUSED_MDLCHECKPARAMETERS
#define UNUSED_MDLOUTPUTS
#define UNUSED_MDLTERMINATE
#include "trailer.c"
