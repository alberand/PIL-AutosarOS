/* Copyright (C) 2013, 2014 Czech Technical University in Prague
 *
 * Authors:
 *     - Carlos Jenkins <carlos@jenkins.co.cr>
 * Modified:
 *     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
 *
 * This document contains proprietary information belonging to Czech
 * Technical University in Prague. Passing on and copying of this
 * document, and communication of its contents is not permitted
 * without prior written authorization.
 *
 * File : sfunction_din.c
 * Abstract:
 *     C-MEX S-function block for digital input.
 *
 * References:
 *     header.c
 *     trailer.c
 *
 * Compile with:
 *     <matlabroot>/bin/mex sfunction_din.c
 */

/*
%YAML 1.2
---
Name: Digital Input
Category: IO blocks
Mnemonic: DIN

Inputs:

Outputs:
  - { name: "Digital Input", type: "bool" }

Parameters:
  - { name: "Pin", type: "uint16"}

# Description and Help is in Markdown mark-up
Description: |

  Gets the digital value of the specified digital input pin on the board.

Help: |

  Digital input
...
*/

#define S_FUNCTION_NAME sfunction_din
#include "header.c"


static void mdlInitializeSizes(SimStruct *S)
{
    /*
     * Configure parameters: 1
     *  - Pin number
     */
    if (!rppSetNumParams(S, 1)) {
        return;
    }

    /*
     * Configure output ports: 1
     *  - Digital input.
     */
    if (!ssSetNumOutputPorts(S, 1)) {
        return;
    }
    rppAddOutputPort(S, 0, SS_BOOLEAN);

    /* Set standard options for this block */
    rppSetStandardOptions(S);
}

#ifdef MATLAB_MEX_FILE
#define MDL_SET_WORK_WIDTHS
static void mdlSetWorkWidths(SimStruct *S)
{
    /* Set number of run-time parameters */
    if (!ssSetNumRunTimeParams(S, 1)) {
        return;
    }

    /* Register the run-time parameter 1 */
    ssRegDlgParamAsRunTimeParam(S, 0, 0, "p1", SS_INT16);
}
#endif

#define UNUSED_MDLCHECKPARAMETERS
#define COMMON_MDLINITIALIZESAMPLETIMES_INHERIT
#define UNUSED_MDLOUTPUTS
#define UNUSED_MDLTERMINATE
#include "trailer.c"
