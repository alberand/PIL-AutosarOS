/* Copyright (C) 2013, 2014 Czech Technical University in Prague
 *
 * Authors:
 *     - Carlos Jenkins <carlos@jenkins.co.cr>
 * Modified:
 *     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
 *
 * This document contains proprietary information belonging to Czech
 * Technical University in Prague. Passing on and copying of this
 * document, and communication of its contents is not permitted
 * without prior written authorization.
 *
 * File : sfunction_scis.c
 * Abstract:
 *     C-MEX S-function block for Serial Communication send.
 *
 * References:
 *     header.c
 *     trailer.c
 *
 * Compile with:
 *     <matlabroot>/bin/mex sfunction_scis.c
 */

/*
%YAML 1.2
---
Name: Serial Send
Category: Communication
Mnemonic: SCIS

Inputs:
  - { name: "Send", type: "uint8" }

Parameters:

# Description and Help is in Markdown mark-up
Description: &desc |

  This block sends a byte to the SCI.

Help: *desc

...
*/

#define S_FUNCTION_NAME sfunction_scis
#include "header.c"


static void mdlInitializeSizes(SimStruct *S)
{
    /*
     * Configure parameters: 0
     */
    if (!rppSetNumParams(S, 0)) {
        return;
    }

    /*
     * Configure input ports: 1
     *  - Data send.
     */
    if (!ssSetNumInputPorts(S, 1)) {
        return;
    }
    rppAddInputPort(S, 0, SS_UINT8);

    /*
     * Configure output ports: 0
     */
    if (!ssSetNumOutputPorts(S, 0)) {
        return;
    }

    /* Set standard options for this block */
    rppSetStandardOptions(S);
}


#define COMMON_MDLINITIALIZESAMPLETIMES_INHERIT
#define UNUSED_MDLCHECKPARAMETERS
#define UNUSED_MDLOUTPUTS
#define UNUSED_MDLTERMINATE
#include "trailer.c"
