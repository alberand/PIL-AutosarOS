/* Copyright (C) 2013, 2014 Czech Technical University in Prague
 *
 * Authors:
 *     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
 *
 * This document contains proprietary information belonging to Czech
 * Technical University in Prague. Passing on and copying of this
 * document, and communication of its contents is not permitted
 * without prior written authorization.
 *
 * File : sfunction_or.c
 * Abstract:
 *     C-MEX S-function block for overrun diagnostics.
 *
 * References:
 *     header.c
 *     trailer.c
 *
 * Compile with:
 *     <matlabroot>/bin/mex sfunction_or.c
 */

/*
%YAML 1.2
---
Name: Overrun Flag
Mnemonic: OR

Inputs:

Outputs:
  - { name: "Overrun Flag",       type: "bool" }

# Description and Help is in Markdown mark-up
Description: |

  Checks if task overrun was detected

Help: |

  Overrun diagnostics
...
*/

#define S_FUNCTION_NAME sfunction_or
#include "header.c"


static void mdlInitializeSizes(SimStruct *S)
{
    /*
     * Configure output ports: 1
     *  - Overrun flag.
     */
    if (!ssSetNumOutputPorts(S, 1)) {
        return;
    }
    rppAddOutputPort(S, 0, SS_BOOLEAN);

    /* Set standard options for this block */
    rppSetStandardOptions(S);
}

#define UNUSED_MDLCHECKPARAMETERS
#define COMMON_MDLINITIALIZESAMPLETIMES_INHERIT
#define UNUSED_MDLOUTPUTS
#define UNUSED_MDLTERMINATE
#include "trailer.c"
