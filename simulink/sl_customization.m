% Copyright (C) 2013-2014 Czech Technical University in Prague
%
% Authors:
%
% This document contains proprietary information belonging to Czech
% Technical University in Prague. Passing on and copying of this
% document, and communication of its contents is not permitted
% without prior written authorization.
%
% File : sl_customization.m

function sl_customization(cm)
    cm.ExtModeTransports.add('autosar_pil.tlc', 'serial', 'ext_serial_win32_comm', 'Level1');
end
