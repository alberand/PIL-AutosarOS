### Simulink Integrity

This folder contains files related to the integration process of project with Simulink.

It can be divided into two main steps:

1. Create Library:
	* Compile blocks
	* Generate library
2. Control of model compilation process
	* Configure model before build
	* Provide scripts and templates for excert of the compilation/uploading process