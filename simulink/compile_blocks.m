% Copyright (C) 2013-2014 Czech Technical University in Prague
%
% Authors:
%     - Carlos Jenkins <carlos@jenkins.co.cr>
% Modified:
%     - Andrey Albershten <albershteyn.andrey@gmail.com>
%
% This document contains proprietary information belonging to Czech
% Technical University in Prague. Passing on and copying of this
% document, and communication of its contents is not permitted
% without prior written authorization.
%
% File : compile_blocks.m
% Abstract:
%     Compile all the C-MEX S-Function in current folder.
%
% References:
%     http://www.mathworks.com/help/matlab/ref/mex.html
%     http://www.mathworks.com/help/matlab/matlab_external/custom-building-mex-files.html


function compile_blocks()
    matlabRootPath=strcat('"',matlabroot(),'"');
    mex = fullfile(matlabRootPath, 'bin', 'mex');

    sources = dir('sfunction_*.c');

    for i = sources'
        command = [mex ' ' i.name];
        disp(command);
        [status, result] = system(command);
        if status ~= 0,
            disp(result)
            throw(MException('pil_mpc5748g:compile_block_failure', [i.name, ' failed to compile']))
        end
    end
end
