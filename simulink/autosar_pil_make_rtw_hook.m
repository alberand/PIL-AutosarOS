% Copyright (C) 2013-2015 Czech Technical University in Prague
%
% Authors:
%     - Carlos Jenkins <carlos@jenkins.co.cr>
% Modified:
%     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
%
% This document contains proprietary information belonging to Czech
% Technical University in Prague. Passing on and copying of this
% document, and communication of its contents is not permitted
% without prior written authorization.
%
% File : rpp_make_rtw_hook.m
% Abstract:
%     Build process hooks file.
%
%     This file is hook file that invoke target-specific functions or
%     executables at specified points in the build process. In particular, this
%     file handle the copying of required files before the compilation stage.
%
% References:
%     rtw_ug.pdf p. 1066-1072 and 1131

function mcal_make_rtw_hook(hookMethod, modelName, rtwRoot, templateMakefile, ...
                           buildOpts, buildArgs)

    switch hookMethod
        case 'error'
            % Called if an error occurs anywhere during the build. If no error
            % occurs during the build, then this hook will not be called. Valid
            % arguments at this stage are hookMethod and modelName. This enables
            % cleaning up any static or global data used by this hook file.
            disp(['### Build procedure for model: ''', modelName, ...
                  ''' aborted due to an error.']);


        case 'entry'
            % Called at start of code generation process (before anything
            % happens).
            % Valid arguments at this stage are hookMethod, modelName, and
            % buildArgs.

            % FIXME: Implement step integrity verification?


        case 'before_tlc'
            % Called just prior to invoking TLC Compiler (actual code
            % generation).
            % Valid arguments at this stage are hookMethod, modelName, and
            % buildArgs.


        case 'after_tlc'
            % Called just after to invoking TLC Compiler (actual code
            % generation).
            % Valid arguments at this stage are hookMethod, modelName, and
            % buildArgs.

            % FIXME: Implement model tasking mode check?


        case 'before_make'
            % Called after code generation is complete, and just prior to
            % kicking off make process (assuming code generation only is not
            % selected). All arguments are valid at this stage.
            
            write_makefiles();

        case 'after_make'
			% Called after make process is complete. All arguments are valid at
            % this stage.

            % Download generated binary to the board
            do_download(modelName);


        case 'exit'
            % Called at the end of the build process.  All arguments are valid
            % at this stage.
            disp(['### Successful completion of build ', ...
                  'procedure for model: ', modelName]);

    end
end

function write_makefiles()

    % Get configuration variables and create paths configuration makefile
    projectRoot = getpref('rpp', 'ProjectRoot');
    TargetRoot   = getpref('rpp', 'TargetRoot');
    buildFolder = fullfile(TargetRoot, 'slprj');
    tgtToolsFile = 'target_tools.mk';
    
    tgtFile = fullfile(buildFolder, tgtToolsFile);
    
    fid = fopen(tgtFile, 'w');
    fwrite(fid, sprintf('%s\n\n', '# Matlab added paths'));
    fwrite(fid, sprintf('PROJECT_ROOT = %s/\n', projectRoot));
    fclose(fid);
end

function do_download(modelName)

    fid = fopen('flash.gdb', 'w+');
    fwrite(fid, sprintf('target remote localhost:7224\n'));
    fwrite(fid, sprintf('monitor _reset\n'));
    fwrite(fid, sprintf('load %s.elf\n', modelName));
    fwrite(fid, sprintf('detach\n'));
    fclose(fid);

    command = sprintf('make -f %s.mk upload', modelName);
    system(command);
end
