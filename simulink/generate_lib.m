% RPP_GENERATE_LIB  Generates rpp_lib.slx library for the current target.

% Copyright (C) 2013-2015 Czech Technical University in Prague
%
% Authors:
%     - Michal Horn <hornmich@fel.cvut.cz>
% Modified:
%     - Andrey Albershteyn <albershteyn.andrey@gmail.com>
%
% This document contains proprietary information belonging to Czech
% Technical University in Prague. Passing on and copying of this
% document, and communication of its contents is not permitted
% without prior written authorization.
%
% File : rpp_generate_lib.m
% Abstract:
%     Generates library rpp_lib.slx for targets
%
% References:
%     http://www.mathworks.com/help/matlab/ref/mex.html
%     http://www.mathworks.com/help/matlab/matlab_external/custom-building-mex-files.html

function rpp_generate_lib(blocks)
    try,
        load_system('pil_mpc5748g.slx');
    catch ME
        new_system('pil_mpc5748g', 'Library');
    end
	set_param('pil_mpc5748g', 'Lock', 'off');
	oldBlocks = find_system('pil_mpc5748g', 'Type', 'block');
	disp('Removing old blocks:');
	for j=1:length(oldBlocks),
		disp(['    ', oldBlocks{j}]);
		delete_block(oldBlocks{j});
	end

	disp('Adding new blocks:');
	for j=1:length(blocks),
		blockFilePath=[blocks{j}, '.slx'];
		disp(['    from: ', blockFilePath, ':']);
		load_system(blockFilePath)
		newBlocks = find_system(blocks{j}, 'Type', 'block');
		for k=1:length(newBlocks),
			[tok, blockName] = strtok(newBlocks{k}, '/');
			blockName = ['pil_mpc5748g', blockName];
			disp(['        ', newBlocks{k}, ' as ', blockName]);

			% Copy rather than link blocks to rpp_lib. This is
			% because we don't want rpp_update_blocks_for_target
			% below to modify the original blocks.
			add_block(newBlocks{k}, blockName, 'LinkStatus', 'none');
		end
		close_system(blockFilePath, 0);
	end

	disp(['Closing and saving file ', 'pil_mpc5748g.slx']);
	set_param('pil_mpc5748g', 'Lock', 'on');
    % j
    save_system('pil_mpc5748g');
	close_system('pil_mpc5748g.slx', 1);
end
