/******************************************************************************
*
*       Freescale(TM) and the Freescale logo are trademarks of Freescale Semiconductor, Inc.
*       All other product or service names are the property of their respective owners.
*       (C) Freescale Semiconductor, Inc. 2018
*
*       THIS SOURCE CODE IS CONFIDENTIAL AND PROPRIETARY AND MAY NOT
*       BE USED OR DISTRIBUTED WITHOUT THE WRITTEN PERMISSION OF
*       Freescale Semiconductor, Inc.
*
*       Description: Configuration Header file
*
*       Note: The implementation that was used is: AUTOSAR_MPC574xG
*       System Generator for AUTOSAR OS/MPC574xG - Version: 4.0 Build 4.0.92
*
********************************************************************************/
#ifndef OSCFG_H
#define OSCFG_H
#define APP_START_SEC_CODE
#include    "Os_memmap.h"

#define OS_START_SEC_CONST_UNSPECIFIED
#include    "Os_sections.h"


/* Applications */
#define OsApplication_0 ((ApplicationType)OS_MKOBJID(OBJECT_APPLICATION, 0U)) /* Application ID */

/* Spinlock */

/* Application modes */
#define Mode01 ((AppModeType)0U)           /* AppMode ID */

/* Common stack */
#define OSMAINTASKSTKSIZE 2072U            /* stack size of MAINTASK */
#define OSMAINTASKSTKBOS OSRUNNABLESTKBEG  /* MAINTASK bos */
#define OSMAINTASKSTKTOS (OSMAINTASKSTKBOS + OSMAINTASKSTKSIZE/4U) /* MAINTASK tos */
#define OSSIMULATIONTASKSTKSIZE 2072U      /* stack size of SIMULATIONTASK */
#define OSSIMULATIONTASKSTKBOS OSMAINTASKSTKTOS /* SIMULATIONTASK bos */
#define OSSIMULATIONTASKSTKTOS (OSSIMULATIONTASKSTKBOS + OSSIMULATIONTASKSTKSIZE/4U) /* SIMULATIONTASK tos */

/* Task definitions */
#define MAINTASK ((TaskType)OS_MKOBJID(OBJECT_TASK, 0U)) /* Task ID */
extern void FuncMAINTASK(void); /* Task entry point */
#define SIMULATIONTASK ((TaskType)OS_MKOBJID(OBJECT_TASK, 1U)) /* Task ID */
extern void FuncSIMULATIONTASK(void); /* Task entry point */
#define EXTMODETASK ((TaskType)OS_MKOBJID(OBJECT_TASK, 2U)) /* Task ID */
extern void FuncEXTMODETASK(void); /* Task entry point */

/* ISR functions */

/* ISRs definition */
#define OSISRAdc_Adcdig_EndGroupConvUnit1() OSISR1DISP(Adc_Adcdig_EndGroupConvUnit1) /* IrqChannel is EXTERNAL */
extern void OS_isr_Adc_Adcdig_EndGroupConvUnit1(void); /* irq: Adc_Adcdig_EndGroupConvUnit1; channel: EXTERNAL; category: 1 */
#define Adc_Adcdig_EndGroupConvUnit1LEVEL 5U /* interrupt level of Adc_Adcdig_EndGroupConvUnit1 */
#define Adc_Adcdig_EndGroupConvUnit1PRIORITY 5U /* priority of Adc_Adcdig_EndGroupConvUnit1 */
#define OSISRLINFLEX_SCI_Isr_UART_RX() OSISR1DISP(LINFLEX_SCI_Isr_UART_RX) /* IrqChannel is EXTERNAL */
extern void OS_isr_LINFLEX_SCI_Isr_UART_RX(void); /* irq: LINFLEX_SCI_Isr_UART_RX; channel: EXTERNAL; category: 1 */
#define LINFLEX_SCI_Isr_UART_RXLEVEL 5U    /* interrupt level of LINFLEX_SCI_Isr_UART_RX */
#define LINFLEX_SCI_Isr_UART_RXPRIORITY 5U /* priority of LINFLEX_SCI_Isr_UART_RX */
#define OSISRLINFLEX_SCI_Isr_UART_TX() OSISR1DISP(LINFLEX_SCI_Isr_UART_TX) /* IrqChannel is EXTERNAL */
extern void OS_isr_LINFLEX_SCI_Isr_UART_TX(void); /* irq: LINFLEX_SCI_Isr_UART_TX; channel: EXTERNAL; category: 1 */
#define LINFLEX_SCI_Isr_UART_TXLEVEL 5U    /* interrupt level of LINFLEX_SCI_Isr_UART_TX */
#define LINFLEX_SCI_Isr_UART_TXPRIORITY 5U /* priority of LINFLEX_SCI_Isr_UART_TX */

/* ISR1 id */
#define Adc_Adcdig_EndGroupConvUnit1 ((ISRType)OS_MKOBJID(OBJECT_ISR, 2U)) /* ISR ID */
#define LINFLEX_SCI_Isr_UART_RX ((ISRType)OS_MKOBJID(OBJECT_ISR, 3U)) /* ISR ID */
#define LINFLEX_SCI_Isr_UART_TX ((ISRType)OS_MKOBJID(OBJECT_ISR, 4U)) /* ISR ID */

/* Resources definitions */
#define OsResource_1 ((ResourceType)OS_MKOBJID(OBJECT_RESOURCE, 0U)) /* Resource ID */
#define RES_SCHEDULER ((ResourceType)OS_MKOBJID(OBJECT_RESOURCE, 1U)) /* Resource ID */

/* Events definition */
#define MainActEvent ((EventMaskType)1U)   /* Event mask */
#define SimActEvent ((EventMaskType)2U)    /* Event mask */

/* Alarms identification */
#define OsAlarm_0 ((AlarmType)OS_MKOBJID(OBJECT_ALARM, 0U)) /* Alarm ID */

/* Counters identification */
#define OsCounter_0 ((CounterType)OS_MKOBJID(OBJECT_COUNTER, 0U)) /* Counter ID */
#define OSMINCYCLE_OsCounter_0 ((TickType)0x1U) /* OsCounter_0 */
#define OSMAXALLOWEDVALUE_OsCounter_0 ((TickType)0xffffU) /* OsCounter_0 */
#define OSTICKSPERBASE_OsCounter_0 10UL    /* OsCounter_0 */
#define OS_TICKS2NS_OsCounter_0(ticks) (PhysicalTimeType)(ticks*250U) /*  */
#define OS_TICKS2US_OsCounter_0(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*250ULL/1000UL) /*  */
#define OS_TICKS2MS_OsCounter_0(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*250ULL/1000000UL) /*  */
#define OS_TICKS2SEC_OsCounter_0(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*250ULL/1000000000UL) /*  */
#define OsCounter_1 ((CounterType)OS_MKOBJID(OBJECT_COUNTER, 1U)) /* Counter ID */
#define OSMINCYCLE_OsCounter_1 ((TickType)0x1U) /* OsCounter_1 */
#define OSMAXALLOWEDVALUE_OsCounter_1 ((TickType)0xffffffffU) /* OsCounter_1 */
#define OSTICKSPERBASE_OsCounter_1 2000UL  /* OsCounter_1 */
#define OS_TICKS2NS_OsCounter_1(ticks) (PhysicalTimeType)(ticks*2625U) /*  */
#define OS_TICKS2US_OsCounter_1(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*2625ULL/1000UL) /*  */
#define OS_TICKS2MS_OsCounter_1(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*2625ULL/1000000UL) /*  */
#define OS_TICKS2SEC_OsCounter_1(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*2625ULL/1000000000UL) /*  */
#define c_time 380952U                     /*  */
#define OSMINCYCLE ((TickType)0x1U)        /* SysTimer */
#define OSMAXALLOWEDVALUE ((TickType)0xffffU) /* SysTimer */
#define OSTICKSPERBASE 10UL                /* SysTimer */
#define OSTICKDURATION 250UL               /* SysTimer */
#define OSMINCYCLE2 ((TickType)0x1U)       /* SecondTimer */
#define OSMAXALLOWEDVALUE2 ((TickType)0xffffffffU) /* SecondTimer */
#define OSTICKSPERBASE2 2000UL             /* SecondTimer */
#define OSTICKDURATION2 2625UL             /* SecondTimer */

/* Messages identification */

/* Flags identification */

/* Message callback prototypes */

/* scheduletable */
#define OS_STOP_SEC_CONST_UNSPECIFIED
#include    "Os_sections.h"

#define APP_STOP_SEC_CODE
#include    "Os_memmap.h"

#endif /* OSCFG_H */

